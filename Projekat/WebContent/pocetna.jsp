<%@page import="paket.Organizacija"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="paket.Korisnik" %>
<%@ page import="paket.Organizacija" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Pocetna strana</title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body>
		<%@ include file="strana/glava.jsp" %>
		
		<%if (session.getAttribute("jeUlogovan") == null || (boolean)session.getAttribute("jeUlogovan") == false){ %><!-- Kada nije ulogovan -->
			<%@ include file="strana/pocetnaNeulogovan.jsp" %>
		<%} else { %>
		
			<% Korisnik k = (Korisnik)session.getAttribute("ulogovan"); %><!-- Kada je ulogovan kao korisnik -->
			<% if(k.getUloga().equals("k")) { %>
				<%@ include file="strana/pocetnaKorisnik.jsp" %>
			<%} else if(k.getUloga().equals("a")) { %><!-- Kada je ulogovan kao admin -->
				<%@ include file="strana/pocetnaAdmin.jsp" %>
			<%} else if(k.getUloga().equals("sa")) { %><!-- Kada je ulogovan kao super admin -->
				<%@ include file="strana/pocetnaSuperAdmin.jsp" %>
			<%} %>
		
		<%} %>
		<br/><br/><br/><br/><br/><br/>
	</body>
</html>
