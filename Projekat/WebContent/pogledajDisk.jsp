<%@page import="paket.Disk"%>
<%@page import="paket.VirtualnaMasina"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<% try{ %>
	<% Korisnik ko = (Korisnik)session.getAttribute("ulogovan"); %>
	<% Disk koga = new Disk("", "", ""); %>
	<% String kogastr = (String)request.getSession().getAttribute("koga"); %>
	<% for(Disk kkkk : Disk.ucitaj()) {%>
	<% 		if(kkkk.getIme().equals(kogastr)){%>
	<% 			koga = kkkk;%>
	<% } 	}%>
	<head>
		<title><%= koga.getIme() %></title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body onload="getPovezan()">
		<%@ include file="strana/glava.jsp" %>
		<div class="unos">
			<h1>Disk <%= koga.getIme() %></h1><br />
			<% request.setAttribute("koga", kogastr); %>
			<% if(ko.getUloga().equals("sa")) {%><form method="POST" action="ObrisiDisk" ><input type="submit" class="sub" value="Obrisi" ></form><%} %>
		</div>
		<form method="POST" action="IzmeniDisk" >
			<div class="unos2">
				<table>
					<tr>
						<th align="right">Ime </th><!-- JEDINSTVENO -->
						<th><input type="text" name="ime" id="ime" value="<%= koga.getIme() %>" <% if(ko.getUloga().equals("k")) {%>disabled<%} %>></th>
					</tr>
					
					<tr>
						<th align="right">Organizacija </th>
						<th><input type="text" name="brj" disabled id="brj" value="<%= koga.getOrg() %>" <% if(ko.getUloga().equals("k")) {%>disabled<%} %>></th>
						<th align="left"><label id="lbrj" style="color: red;"></label></th>
					</tr>
					
					<% if(koga.getTip().toUpperCase().equals("SSD")){ %>

					<tr>
						<th align="right">Tip </th>
						<th align="left"><select name="tip" id="tip" <% if(ko.getUloga().equals("k")) {%>disabled<%} %>>
							<option value="SSD">SSD</option>
							<option value="HDD">HDD</option>
						</select></th>
						<th></th>
					</tr>
					
					<% }else{ %>
					
					<tr>
						<th align="right">Tip </th>
						<th align="left"><select name="tip" id="tip" <% if(ko.getUloga().equals("k")) {%>disabled<%} %>>
							<option value="HDD">HDD</option>
							<option value="SSD">SSD</option>
						</select></th>
						<th></th>
					</tr>
					
					<%} %>
					
					<tr>
						<th align="right">Kapacitet </th>
						<th><input type="number" step=".1" value="<%= koga.getKapacitet() %>" name="kapacitet" id="kapacitet" <% if(ko.getUloga().equals("k")) {%>disabled<%} %>></th>
						<th align="left"><label id="lkapacitet" style="color: red;"></label></th>
					</tr>
					
					<tr>
						<th align="right">Povezan sa </th>
						<th align="left" id="povezan">
							
						</th>
					</tr>
					
					<tr>
						<th></th>
						<%if(!ko.getUloga().equals("k")){ %>
						<th align="left"><input type="submit" class="sub" value="Sacuvaj Promene"></th>
						<%} %>
						<th align="left"><label style="color: red;">
							<% if(request.getAttribute("poruka")!=null){ %>
							<%= request.getAttribute("poruka") %>
							<% }%>
						</label></th>
					</tr>
				</table>
			</div>
		</form>
	</body>
	<script>
			function getPovezan(){
				<% if(ko.getUloga().equals("k")) { String ssss = "<input type=\"text\" value=\"" + koga.getVm() + "\" name=\"VMasina\" id=\"VMasina\" disabled>"; %>
					var veze = ' <%= ssss %> ';
				<%} else{ %>
					var veze = '<%= VirtualnaMasina.vratiVeze(koga.getIme()) %>';
				<%} %>
				document.getElementById("povezan").innerHTML = veze
			}
		</script>
<% } catch(Exception e){ response.sendError(403); } %>
</html>