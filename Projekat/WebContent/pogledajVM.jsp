<%@page import="paket.Disk"%>
<%@page import="paket.Kategorija"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="paket.Korisnik" %>
<%@ page import="paket.VirtualnaMasina" %>
<%@ page import="paket.Kategorija" %>
<%@ page import="paket.Organizacija" %>
<%@ page import="java.util.Arrays" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<% try{ %>
	<% Korisnik ko = (Korisnik)session.getAttribute("ulogovan"); %>
	<% Kategorija kat = new Kategorija("Greska", 0, 0.0); %>
	<% VirtualnaMasina koga = new VirtualnaMasina("","", kat); %>
	<% String kogastr = (String)request.getSession().getAttribute("koga"); %>
	<% for(VirtualnaMasina kkkk : VirtualnaMasina.ucitaj()) {%>
	<% 		if(kkkk.getIme().equals(kogastr)){%>
	<% 			koga = kkkk;%>
	<% } 	}%>
	<% if (koga.getIme().equals("")) {response.sendError(400);}%>
	<head>
		<title><%= koga.getIme() %></title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body onload="prikazDisk()">
		<%@ include file="strana/glava.jsp" %>
		<div class="unos">
			<h1><%= koga.getIme() %></h1><br />
			<% request.setAttribute("koga", kogastr); %>
			<label style="color: red;">
							<% if(request.getAttribute("porukaa")!=null){ %>
							<%= request.getAttribute("porukaa") %>
							<% }%>
						</label>
			<% if(!ko.getUloga().equals("k")) {%><form method="POST" action="UpaliUgasiVM" ><input  type="submit" class="sub" <% if(koga.getAktivnosti().length % 2 == 0 || Arrays.toString(koga.getAktivnosti()).contains("[ ]") || Arrays.toString(koga.getAktivnosti()).contains("[]") || koga.getAktivnosti()[0] == "" || koga.getAktivnosti()[0].equals(" ")){ %> style="background-color:lime;" value="Upali" <%} else{ %>style="background-color:red;" value="Ugasi"<%} %> ></form> <%} %>
			<% if(ko.getUloga().equals("sa")) {%><form method="POST" action="ObrisiVM" ><input type="submit" class="sub" value="Obrisi" ></form><%} %>
		</div>
		<form method="POST" action="IzmeniVM" onSubmit="return proveri();" >
			<div class="unos2">
				<table>
					<tr>
						<th align="right">Ime </th>
						<th><input type="text" name="ime" id="ime" <% if(ko.getUloga().equals("k")) {%>disabled<%} %> value="<%= koga.getIme() %>"></th>
						<th align="left"><label id="lime" style="color: red;"></label></th>
					</tr>
					<% if(ko.getUloga().equals("sa")) {%>
					<tr>
						<th align="right">Organizacija</th>
						<th><input type="text" name="organizacija" id="organizacija" disabled value="<%= koga.getOrganizacija() %>"></th>
						<th align="left"><label id="lorganizacija" style="color: red;"></label></th>
					</tr>
					
					<tr>
						<th align="right">Kategorija </th>
						<th align="left"><select name="kat" id="kat">
						<option value="<%= koga.getKategorija().getIme() %>"><%= koga.getKategorija().getIme() %></option>
						<% for( Kategorija kateg : Kategorija.ucitaj()) { if ( !kateg.getIme().equals(koga.getKategorija().getIme()) ){%>
							<option value="<%= kateg.getIme() %>"><%= kateg.getIme() %></option>
						<%} } %>
						</select></th>
						<th align="left"><label id="lkat" style="color: red;"></label></th>
					</tr>
					<%} else {%>
						<tr>
							<th align="right">Organizacija</th>
							<th><input type="text" name="organizacija" id="organizacija" disabled value="<%= koga.getOrganizacija() %>"></th>
							<th align="left"><label id="lorganizacija" style="color: red;"></label></th>
						</tr>
						
						<tr>
							<th align="right">Kategorija</th>
							<th><input type="text" name="kat" id="kat" disabled value="<%= koga.getKategorija().getIme() %>"></th>
							<th align="left"><label id="lkat" style="color: red;"></label></th>
						</tr>
					<%} %>
					<tr>
						<th align="right">Broj Jezgara </th>
						<th><input type="text" name="brj" id="brj" disabled value="<%= koga.getKategorija().getBrJezgra() %>"></th>
						<th align="left"><label id="lbrj" style="color: red;"></label></th>
					</tr>
					<tr>
						<th align="right">RAM </th>
						<th><input type="text" name="ram" id="ram" disabled value="<%= koga.getKategorija().getRam() %>"></th>
						<th align="left"><label id="lram" style="color: red;"></label></th>
					</tr>
					<tr>
						<th align="right">Broj GPU Jezgara </th>
						<th><input type="text" name="bgj" id="bgj" disabled value="<%= koga.getKategorija().getBrGPUJezgra() %>"></th>
						<th align="left"><label id="lbaj" style="color: red;"></label></th>
					</tr>
					<tr>
						<th align="right">Diskovi </th>
						<th align="left" id="VMs">
						<%int i = 1; %>
						<% for (String s : koga.getDiskovi()){ %>
							<input type="text" name="disk<%= i %>" id="disk<%= i %>" <% if(ko.getUloga().equals("k")) {%>disabled<%} %> value="<%= s %>"><br />
						<% ++i; } %>
						</th>
						<th align="left"><label id="ldisk" style="color: red;"></label></th>
					</tr>
					<tr>
						<th align="right">Aktivnosti </th>
						<th align="left">
						<% i = 0; %>
						<% try{for (String s : koga.getAktivnosti()){
							
							String[] L = s.split("T");
							String[] D = L[0].split("-");
							String[] D2 = L[1].split("//.");
							String[] H = D2[0].split(":");
							
							String sd = D[2] + "/" + D[1] + "/" + D[0] + " u " + H[0] + ":" + H[1];
							
							if(i++ % 2 == 0){ %>
							Upaljen <%= sd %>
						<% }else{ %>
							Ugasen <%= sd %><br />
						<% } }}catch(Exception e){%> <i>[Nema aktivnosti]</i> <%} %>
						</th>
						<th align="left"><label id="laktivnost" style="color: red;"></label></th>
					</tr>
					<tr>
						<th></th>
						<%if(!ko.getUloga().equals("k")){ %>
						<th align="left"><input type="submit" class="sub" value="Sacuvaj Promene"></th>
						<% } %>
						<th align="left"><label style="color: red;">
							<% if(request.getAttribute("poruka")!=null){ %>
							<%= request.getAttribute("poruka") %>
							<% }%>
						</label></th>
					</tr>
				</table>
			</div>
		</form>
		<script>
			function prikazDisk(){
				<% if(ko.getUloga().equals("k")) {%>
					<%
						String di = "";
						for(Disk diss : Disk.ucitaj())
							if(diss.getVm().equals(koga.getIme()))
								di += diss.getIme() + "<br />";
					%>
				
					var disk = '<%= di %>';
				<%} else{ %>
					var disk = '<%= VirtualnaMasina.vratiOznaceneDiskove(koga.getIme()) %>';
				<%} %>
				document.getElementById("VMs").innerHTML = disk;
				
				
			}
		</script>
	</body>
<% } catch(Exception e){ response.sendError(403); } %>
</html>