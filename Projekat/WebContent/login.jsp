<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="paket.Korisnik" %>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Login</title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body>
		<%@ include file="strana/glava.jsp" %>
		<% if (session.getAttribute("jeUlogovan") == null || (boolean)session.getAttribute("jeUlogovan") == false){ %>
			<form method="POST" action="Login" onSubmit="return proveri();">
				<div class="unos">
					<table>
						<tr>
							<th align="right">Email </th>
							<th><input type="text" name="email" id="email"></th>
							<th align="left"><label id="lemail" style="color: red;"></label></th>
						</tr>
						<tr>
							<th align="right">Password </th>
							<th><input type="password" name="password" id="password"></th>
							<th align="left"><label id="lpassword" style="color: red;"></label></th>
						</tr>
						<tr>
							<th></th>
							<th align="left"><input type="submit" class="sub" value="Uloguj se"></th>
							<th align="left"><label id="lpassword" style="color: red;">
							<% if(request.getAttribute("poruka")!=null){ %>
							<%= request.getAttribute("poruka") %>
							<% }%>
							</label></th>
						</tr>
					</table>
				</div>
			</form>
		<%} else{%>
			<div class="unos">Vec ste ulogovani</div>
		<%} %>
		<script>
			function proveri() {
				var popunjeno = true;

				if(document.getElementById("email").value == ""){
					popunjeno = false
					document.getElementById('lemail').innerHTML = 'Ovo polje je obavezno!';
				}
				else if(document.getElementById("email").value.includes("@") == false || document.getElementById("email").value.includes(".") == false){
					popunjeno = false
					document.getElementById('lemail').innerHTML = 'Email koji ste uneli je pogresan!';
				}
				else{
					document.getElementById('lemail').innerHTML = '';
				}
						  
				if(document.getElementById("password").value == ""){
					popunjeno = false
					document.getElementById('lpassword').innerHTML = 'Ovo polje je obavezno!';
				}
				else{
					document.getElementById('lpassword').innerHTML = '';
				}
					  
				if(!popunjeno) {alert("Morate da pravilno popunite sva polja")}
				return popunjeno;
			}
		</script>
	</body>
</html>