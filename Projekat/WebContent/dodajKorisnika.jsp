<%@page import="paket.Organizacija"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="paket.Korisnik" %>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Dodaj novog korisnika</title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body>
	<%@ include file="strana/glava.jsp" %>
	<% if(session.getAttribute("jeUlogovan") != null){ Korisnik k = (Korisnik)session.getAttribute("ulogovan"); if(!k.getUloga().equals("k")) { %>
		<form method="POST" action="DodajKorisnika" onSubmit="return proveri();">
		<div class="unos">
				<table>
					<tr>
						<th align="right">Email </th>
						<th><input type="text" name="email" id="email"></th>
						<th align="left"><label id="lemail" style="color: red;"></label></th>
					</tr>
					<tr>
						<th align="right">Password </th>
						<th><input type="text" name="password" id="password"></th>
						<th align="left"><label id="lpassword" style="color: red;"></label></th>
					</tr>
					<tr>
						<th align="right">Ime </th>
						<th><input type="text" name="ime" id="ime"></th>
						<th align="left"><label id="lime" style="color: red;"></label></th>
					</tr>
					<tr>
						<th align="right">Prezime </th>
						<th><input type="text" name="prezime" id="prezime"></th>
						<th align="left"><label id="lprezime" style="color: red;"></label></th>
					</tr>
					<%if(k.getUloga().equals("sa")) {%>
					<tr>
						<th align="right">Organizacija </th>
						
						<th align="left"><select name="organizacija" id="organizacija">
						<% for( Organizacija org : Organizacija.ucitaj()) {%>
							<option value="<%= org.getIme() %>"><%= org.getIme() %></option>
						<%} %>
						</select></th>
						<th align="left"><label id="lorganizacija" style="color: red;"></label></th>
					</tr>
					<% } %>
					<tr>
						<th align="right">Uloga </th>
						<th align="left"><select name="uloga" id="uloga">
							<option value="k">Korisnik</option>
							<option value="a">Admin</option>
						</select></th>
						<th></th>
					</tr>
					<tr>
						<th></th>
						<th align="left"><input type="submit" class="sub" value="Dodaj"></th>
						<th align="left"><label style="color: red;">
							<% if(request.getAttribute("poruka")!=null){ %>
							<%= request.getAttribute("poruka") %>
							<% }%>
						</label></th>
					</tr>
				</table>
			</div>
		</form>
		<script>
			function proveri() {
				var popunjeno = true;

				if(document.getElementById("email").value == ""){
					popunjeno = false
					document.getElementById('lemail').innerHTML = 'Ovo polje je obavezno!';
				}
				else if(document.getElementById("email").value.includes("@") == false || document.getElementById("email").value.includes(".") == false){
					popunjeno = false
					document.getElementById('lemail').innerHTML = 'Email koji ste uneli je pogresan!';
				}
				else{
					document.getElementById('lemail').innerHTML = '';
				}
						  
				if(document.getElementById("password").value == ""){
					popunjeno = false
					document.getElementById('lpassword').innerHTML = 'Ovo polje je obavezno!';
				}
				else if(document.getElementById("password").value.length < 8){
					popunjeno = false
					document.getElementById('lpassword').innerHTML = 'Lozinka mora da sadrzi minimalno 8 karaktera!';
				}
				else{
					document.getElementById('lpassword').innerHTML = '';
				}
				
				if(document.getElementById("ime").value == ""){
					popunjeno = false
					document.getElementById('lime').innerHTML = 'Ovo polje je obavezno!';
				}
				else{
					document.getElementById('lime').innerHTML = '';
				}
						
				if(document.getElementById("prezime").value == ""){
					popunjeno = false
					document.getElementById('lprezime').innerHTML = 'Ovo polje je obavezno!';
				}
				else{
					document.getElementById('lprezime').innerHTML = '';
				}
				<% if(k.getUloga().equals("sa")){ %>
				if(document.getElementById("organizacija").value == ""){
					popunjeno = false
					document.getElementById('lorganizacija').innerHTML = 'Ovo polje je obavezno!';
				}
				else{
					document.getElementById('lorganizacija').innerHTML = '';
				}
				<%} %>
					  
				if(!popunjeno) {alert("Morate da pravilno popunite sva polja")}
				return popunjeno;
			}
		</script>
		<%} else{%>
			<div class="unos">Nemate pravo da dodajete korisnike</div>
		<% }} else{%>
			<div class="unos">Nemate pravo da dodajete korisnike</div>
		<%}%>
	</body>
</html>