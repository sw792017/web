<%@page import="paket.VirtualnaMasina"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="paket.Korisnik" %>
<%@ page import="paket.Organizacija" %>
<%@ page import="paket.Kategorija" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Dodaj Disk</title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body>
		<%@ include file="strana/glava.jsp" %>
	<% if(session.getAttribute("jeUlogovan") != null){ Korisnik k = (Korisnik)session.getAttribute("ulogovan"); if(!k.getUloga().equals("k")) { %>
		<form method="POST" action="DodajDisk" onSubmit="return proveri();">
		<div class="unos">
				<table>
					<tr>
						<th align="right">Ime </th><!-- JEDINSTVENO -->
						<th><input type="text" name="ime" id="ime"></th>
						<th align="left"><label id="lime" style="color: red;"></label></th>
					</tr>
					
					<%if(k.getUloga().equals("sa")) {%>
					<tr>
						<th align="right">Organizacija </th>
						
						<th align="left"><select name="organizacija" id="organizacija">
						<% for( Organizacija org : Organizacija.ucitaj()) {%>
							<option value="<%= org.getIme() %>"><%= org.getIme() %></option>
						<%} %>
						</select></th>
						<th align="left"><label id="lorganizacija" style="color: red;"></label></th>
					</tr>
					<% } %>

					<tr>
						<th align="right">Tip </th>
						<th align="left"><select name="tip" id="tip">
							<option value="HDD">HDD</option>
							<option value="SSD">SSD</option>
						</select></th>
						<th></th>
					</tr>
					
					<tr>
						<th align="right">Kapacitet </th>
						<th><input type="number" step=".1" name="kapacitet" id="kapacitet"></th>
						<th align="left"><label id="lkapacitet" style="color: red;"></label></th>
					</tr>

					<tr>
						<th></th>
						<th align="left"><input type="submit" class="sub" value="Dodaj"></th>
						<th align="left"><label id="lsub" style="color: red;">
							<% if(request.getAttribute("poruka")!=null){ %>
							<%= request.getAttribute("poruka") %>
							<% }%>
						</label></th>
					</tr>
				</table>
			</div>
		</form>
		<script>
			function proveri() {
				var popunjeno = true;

				if(document.getElementById("ime").value == ""){
					popunjeno = false
					document.getElementById('lime').innerHTML = 'Ovo polje je obavezno!';
				}
				else{
					document.getElementById('lime').innerHTML = '';
				}
				
				if(document.getElementById("kapacitet").value < 0.1){
					popunjeno = false
					document.getElementById('lkapacitet').innerHTML = 'Ne mozete imati manje od 0.1 GB kapaciteta!';
				}
				else{
					document.getElementById('lkapacitet').innerHTML = '';
				}
					  
				if(!popunjeno) {alert("Morate da pravilno popunite sva polja")}
				return popunjeno;
			}
		</script>
		<%} else{%>
			<div class="unos">Nemate pravo da dodajete virtualne masine</div>
		<% }} else{%>
			<div class="unos">Nemate pravo da dodajete virtualne masine</div>
		<%}%>
	</body>
</html>