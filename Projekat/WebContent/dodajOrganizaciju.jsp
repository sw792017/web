<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="paket.Korisnik" %>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Dodaj Organizaciju</title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body>
		<%@ include file="strana/glava.jsp" %>
	<% if(session.getAttribute("jeUlogovan") != null){ Korisnik k = (Korisnik)session.getAttribute("ulogovan"); if(k.getUloga().equals("sa")) { %>
		<form method="POST" action="DodajOrganizaciju" onSubmit="return proveri();">
		<div class="unos">
				<table>
					<tr>
						<th align="right">Ime </th><!-- JEDINSTVENO -->
						<th><input type="text" name="ime" id="ime"></th>
						<th align="left"><label id="lime" style="color: red;"></label></th>
					</tr>
					<tr>
						<th align="right">Opis </th>
						<th><input type="text" name="opis" id="opis"></th>
						<th></th>
					</tr>
					<tr>
						<th align="right">Logo </th>
						<th><input type="file" name="logo" id="logo" accept="image/*"></th>
						<th></th>
					</tr>
					<tr>
						<th></th>
						<th align="left"><input type="submit" class="sub" value="Dodaj"></th>
						<th align="left"><label id="lpassword" style="color: red;">
							<% if(request.getAttribute("poruka")!=null){ %>
							<%= request.getAttribute("poruka") %>
							<% }%>
						</label></th>
					</tr>
				</table>
			</div>
		</form>
		<script>
			function proveri() {
				var popunjeno = true;

				if(document.getElementById("ime").value == ""){
					popunjeno = false
					document.getElementById('lime').innerHTML = 'Ovo polje je obavezno!';
				}
				else{
					document.getElementById('lime').innerHTML = '';
				}
					  
				if(!popunjeno) {alert("Morate da pravilno popunite sva polja")}
				return popunjeno;
			}
		</script>
		<%} else{%>
			<div class="unos">Nemate pravo da dodajete organizacije</div>
		<% }} else{%>
			<div class="unos">Nemate pravo da dodajete organizacije</div>
		<%}%>
	</body>
</html>