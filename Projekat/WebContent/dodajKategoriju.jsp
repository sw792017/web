<%@page import="paket.VirtualnaMasina"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="paket.Korisnik" %>
<%@ page import="paket.Organizacija" %>
<%@ page import="paket.Kategorija" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Dodaj Kategoriju</title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body>
		<%@ include file="strana/glava.jsp" %>
	<% if(session.getAttribute("jeUlogovan") != null){ Korisnik k = (Korisnik)session.getAttribute("ulogovan"); if(!k.getUloga().equals("k")) { %>
		<form method="POST" action="DodajKategoriju" onSubmit="return proveri();">
		<div class="unos">
				<table>
					<tr>
						<th align="right">Ime </th><!-- JEDINSTVENO -->
						<th><input type="text" name="ime" id="ime"></th>
						<th align="left"><label id="lime" style="color: red;"></label></th>
					</tr>
					
					<tr>
						<th align="right">Broj jezgara </th>
						<th><input type="number" name="brj" id="brj"></th>
						<th align="left"><label id="lbrj" style="color: red;"></label></th>
					</tr>

					<tr>
						<th align="right">Broj GPU jezgara </th>
						<th><input type="number" name="brgj" id="brgj"></th>
						<th align="left"><label id="lbrgj" style="color: red;"></label></th>
					</tr>
					
					<tr>
						<th align="right">Gigabajta RAMa </th>
						<th><input type="number" step=".1" name="ram" id="ram"></th>
						<th align="left"><label id="lram" style="color: red;"></label></th>
					</tr>

					<tr>
						<th></th>
						<th align="left"><input type="submit" class="sub" value="Dodaj"></th>
						<th align="left"><label id="lpassword" style="color: red;">
							<% if(request.getAttribute("poruka")!=null){ %>
							<%= request.getAttribute("poruka") %>
							<% }%>
						</label></th>
					</tr>
				</table>
			</div>
		</form>
		<script>
			function proveri() {
				var popunjeno = true;

				if(document.getElementById("ime").value == ""){
					popunjeno = false
					document.getElementById('lime').innerHTML = 'Ovo polje je obavezno!';
				}
				else{
					document.getElementById('lime').innerHTML = '';
				}
				
				if(document.getElementById("brj").value < 1){
					popunjeno = false
					document.getElementById('lbrj').innerHTML = 'Ne mozete imati manje od 1 CPU jezgra!';
				}
				else{
					document.getElementById('lbrj').innerHTML = '';
				}
				
				if(document.getElementById("brgj").value < 0){
					popunjeno = false
					document.getElementById('lbrgj').innerHTML = 'Ne mozete imati negativan broj GPU jezgra!';
				}
				else{
					document.getElementById('lbrgj').innerHTML = '';
				}
				
				if(document.getElementById("ram").value <= 0){
					popunjeno = false
					document.getElementById('lram').innerHTML = 'Ne mozete imati manje od 0GB rama!';
				}
				else{
					document.getElementById('lram').innerHTML = '';
				}
					  
				if(!popunjeno) {alert("Morate da pravilno popunite sva polja")}
				return popunjeno;
			}
		</script>
		<%} else{%>
			<div class="unos">Nemate pravo da dodajete virtualne masine</div>
		<% }} else{%>
			<div class="unos">Nemate pravo da dodajete virtualne masine</div>
		<%}%>
	</body>
</html>