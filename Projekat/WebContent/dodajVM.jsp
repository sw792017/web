<%@page import="paket.VirtualnaMasina"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="paket.Korisnik" %>
<%@ page import="paket.Organizacija" %>
<%@ page import="paket.Kategorija" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Dodaj Organizaciju</title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body onload="load()">
		<%@ include file="strana/glava.jsp" %>
	<% if(session.getAttribute("jeUlogovan") != null){ Korisnik k = (Korisnik)session.getAttribute("ulogovan"); if(!k.getUloga().equals("k")) { %>
		<form method="POST" action="DodajVM" onSubmit="return proveri();">
		<div class="unos">
				<table>
					<tr>
						<th align="right">Ime </th><!-- JEDINSTVENO -->
						<th><input type="text" name="ime" id="ime"></th>
						<th align="left"><label id="lime" style="color: red;"></label></th>
					</tr>
					
					<tr>
						<th align="right">Kategorija </th>
						
						<th align="left"><select onchange="getkategorija()" name="kategorija" id="kategorija">
						<% for( Kategorija kat : Kategorija.ucitaj()) {%>
							<option value="<%= kat.getIme() %>"><%= kat.getIme() %></option>
						<%} %>
						</select></th>
						<th align="left"><label id="lkategorija" style="color: red;"></label></th>
					</tr>
					
					<tr id="brjtr">
						<th align="right">Broj Jezgara </th><!-- AUTOMATSKO -->
						<th><input type="text" id="brj" disabled></th>
					</tr>
					
					<tr id="ramtr">
						<th align="right">Gigabajta RAMa </th><!-- AUTOMATSKO -->
						<th><input type="text" id="ram" disabled></th>
					</tr>
					
					<tr id="bgjtr">
						<th align="right">Broj GPU Jezgara </th><!-- AUTOMATSKO -->
						<th><input type="text" id="brgj" disabled></th>
					</tr>
					
					<%if(k.getUloga().equals("sa")) {%>
					<tr>
						<th align="right">Organizacija </th>
						
						<th align="left"><select onchange="prikazDisk()" name="organizacija" id="organizacija">
						<% for( Organizacija org : Organizacija.ucitaj()) {%>
							<option value="<%= org.getIme() %>"><%= org.getIme() %></option>
						<%} %>
						</select></th>
						<th align="left"><label id="lorganizacija" style="color: red;"></label></th>
					</tr>
					<% } %>
					
					<tr>
						<th></th>
						<th align="left" id="VMs"></th>
					</tr>

					<tr>
						<th></th>
						<th align="left"><input type="submit" class="sub" value="Dodaj"></th>
						<th align="left"><label id="lpassword" style="color: red;">
							<% if(request.getAttribute("poruka")!=null){ %>
							<%= request.getAttribute("poruka") %>
							<% }%>
						</label></th>
					</tr>
				</table>
			</div>
		</form>
		<script>
			function getkategorija(){
				var BrJezgra = <%= VirtualnaMasina.vratiPoKategorijiBrJezgra() %>;
				var BrGPUJezgra = <%= VirtualnaMasina.vratiPoKategorijiBrGPUJezgra() %>;
				var Ram = <%= VirtualnaMasina.vratiPoKategorijiRAM() %>;
				
				var index = document.getElementById("kategorija").selectedIndex;
				
				document.getElementById("brj").value = BrJezgra[index];
				document.getElementById("ram").value = Ram[index];
				document.getElementById("brgj").value = BrGPUJezgra[index];
			}
			
			function prikazDisk(){
				var disk = <%= VirtualnaMasina.vratiDiskove() %>;
				
				<% if(k.getUloga().equals("sa")){ %>
					var index = document.getElementById("organizacija").selectedIndex;
				<% }else{ %>
					<%
					int i = 0;
					for(Organizacija org : Organizacija.ucitaj())
						if(org.getIme().equals(k.getOrganizacija()))
							break;
					%>
					var index = <%= i %>;
				<% } %>
				
				document.getElementById("VMs").innerHTML = disk[index];
			}
			
			function load() {
				getkategorija();
				prikazDisk();
			}
			
			function proveri() {
				var popunjeno = true;

				if(document.getElementById("ime").value == ""){
					popunjeno = false
					document.getElementById('lime').innerHTML = 'Ovo polje je obavezno!';
				}
				else{
					document.getElementById('lime').innerHTML = '';
				}
					  
				if(!popunjeno) {alert("Morate da pravilno popunite sva polja")}
				return popunjeno;
			}
		</script>
		<%} else{%>
			<div class="unos">Nemate pravo da dodajete virtualne masine</div>
		<% }} else{%>
			<div class="unos">Nemate pravo da dodajete virtualne masine</div>
		<%}%>
	</body>
</html>