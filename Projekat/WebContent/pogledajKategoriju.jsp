<%@page import="paket.Kategorija"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<% try{ %>
	<% Korisnik ko = (Korisnik)session.getAttribute("ulogovan"); %>
	<% Kategorija koga = new Kategorija("", 0, 0); %>
	<% String kogastr = (String)request.getSession().getAttribute("koga"); %>
	<% for(Kategorija kkkk : Kategorija.ucitaj()) {%>
	<% 		if(kkkk.getIme().equals(kogastr)){%>
	<% 			koga = kkkk;%>
	<% } 	}%>
	<head>
		<title><%= koga.getIme() %></title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body>
		<%@ include file="strana/glava.jsp" %>
		<div class="unos">
			<h1><%= koga.getIme() %></h1><br />
			<% request.setAttribute("koga", kogastr); %>
			<% if(ko.getUloga().equals("sa")) {%><form method="POST" action="ObrisiKategoriju" ><input type="submit" class="sub" value="Obrisi" ></form><%} %>
		</div>
		<form method="POST" action="IzmeniKategoriju" >
			<div class="unos2">
				<table>
					<tr>
						<th align="right">Ime </th><!-- JEDINSTVENO -->
						<th><input type="text" name="ime" id="ime" value="<%= koga.getIme() %>"></th>
					</tr>
					
					<tr>
						<th align="right">Broj jezgara </th>
						<th><input type="number" name="brj" id="brj" value="<%= koga.getBrJezgra() %>"></th>
						<th align="left"><label id="lbrj" style="color: red;"></label></th>
					</tr>

					<tr>
						<th align="right">Broj GPU jezgara </th>
						<th><input type="number" name="brgj" id="brgj" value="<%= koga.getBrGPUJezgra() %>"></th>
						<th align="left"><label id="lbrgj" style="color: red;"></label></th>
					</tr>
					
					<tr>
						<th align="right">Gigabajta RAMa </th>
						<th><input type="number" name="ram" step=".1" id="ram" value="<%= koga.getRam() %>"></th>
						<th align="left"><label id="lram" style="color: red;"></label></th>
					</tr>
					<tr>
						<th></th>
						<th align="left"><input type="submit" class="sub" value="Sacuvaj Promene"></th>
						<th align="left"><label style="color: red;">
							<% if(request.getAttribute("poruka")!=null){ %>
							<%= request.getAttribute("poruka") %>
							<% }%>
						</label></th>
					</tr>
				</table>
			</div>
		</form>
	</body>
<% } catch(Exception e){ response.sendError(403); } %>
</html>