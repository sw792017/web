<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    <%@ page import="paket.Korisnik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script src="js/sneg.js"></script>
<%if (session.getAttribute("jeUlogovan") == null || (boolean)session.getAttribute("jeUlogovan") == false){ %><!-- Kada nije ulogovan -->
	<ul>

	</ul>
<%} else { %>

	<% Korisnik k = (Korisnik)session.getAttribute("ulogovan"); %><!-- Kada je ulogovan kao korisnik -->
	<% if(k.getUloga().equals("k")) { %>
		<ul>
			<li><form method="POST" action="PocetnaStrana" ><input type="submit" value="Pocetna Strana"></form></li>
			<li><form method="POST" action="PogledajOrganizaciju" ><input name="<%= k.getOrganizacija() %>" type="submit" class="meni" value="Pogjedaj Svoju Organizaciju" ></form></li>
			<li><form method="POST" action="IzlogujSe" ><input type="submit" class="meni" value="Izloguj se" ></form></li>
			
			<li class="desno"><form method="POST" action="Omeni" ><input type="submit" class="meni" value="<%= k.getIme() %> <%= k.getPrezime() %>" ></form></li>
		</ul>
	<%} else if(k.getUloga().equals("a")) { %><!-- Kada je ulogovan kao admin -->
		<ul>
			<li><form method="POST" action="PocetnaStrana" ><input type="submit" value="Pocetna Strana"></form></li>
			<li><form method="POST" action="PogledajOrganizaciju" ><input name="<%= k.getOrganizacija() %>" type="submit" class="meni" value="Pogjedaj Svoju Organizaciju" ></form></li>
			<li><form method="POST" action="IzlogujSe" ><input type="submit" class="meni" value="Izloguj se" ></form></li>
			
			<li class="desno"><form method="POST" action="Omeni" ><input type="submit" class="meni" value="<%= k.getIme() %> <%= k.getPrezime() %>" ></form></li>
		</ul>
	<%} else if(k.getUloga().equals("sa")) { %><!-- Kada je ulogovan kao super admin -->
		<ul>
			<li><form method="POST" action="PocetnaStrana" ><input type="submit" value="Pocetna Strana"></form></li>
			<li><form method="POST" action="IzlogujSe" ><input type="submit" class="meni" value="Izloguj se" ></form></li>
			
			<li class="desno"><form method="POST" action="Omeni" ><input type="submit" class="meni" value="<%= k.getIme() %> <%= k.getPrezime() %>" ></form></li>
		</ul>
	<%} %>

<%} %>
