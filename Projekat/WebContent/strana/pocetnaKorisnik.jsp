<%@page import="paket.Kategorija"%>
<%@page import="paket.VirtualnaMasina"%>
<%@page import="paket.Organizacija"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="paket.Korisnik" %>
<%@ page import="paket.Organizacija" %>
<%@ page import="paket.Disk" %>

	<div class="unos">
		<h2>VIRTUALNE MASINE</h2>
		Broj CPU jezgra od <input style="" class="avs" type="number" id="odCore" onchange="tt()"> do <input class="avs" type="number" id="doCore" onchange="tt()"><br />
		Broj GPU jezgra od <input type="number" class="avs" id="odgcore" onchange="tt()"> do <input type="number" class="avs" id="dogcore" onchange="tt()"><br />
		Gigabajta RAMa od <input type="number" class="avs" step="0.1" id="odRAM" onchange="tt()"> do <input class="avs" type="number" step="0.1" id="doRAM" onchange="tt()"><br />
		<table border="1" class="table" id="tabb">
		    <tr class="tr">
		        <th class="th">ID</th>
		        <th class="th">Ime</th>
		        <th class="th">Kategorija</th>
		        <th class="th">Br. Jezgara</th>
		        <th class="th">RAM</th>
		        <th class="th">Br. GPU Jezgara</th>
		        <th class="th"></th>
		    </tr>
		    <% int indexx = 0; %>
		    <% for( VirtualnaMasina kk : VirtualnaMasina.ucitaj()) {if(kk.getOrganizacija().equals(k.getOrganizacija())){%>
		    <tr <% if(indexx%2 == 0){%> class="tr" <% } else{ %> class="even" <%} %>>
		        <td class="td">
		            <%= ++indexx %>
		        </td>
		        <td class="td">
		            <%= kk.getIme() %>
		        </td>
		        <td class="td">
		            <%= kk.getKategorija().getIme() %>
		        </td>
		        <td class="td">
		        	<%= kk.getBrJezgra() %>
		        </td>
		        <td class="td">
		        	<%= kk.getRam() %>
		        </td>
		        <td class="td">
		        	<%= kk.getBrGPUJezgra() %>
		        </td>

		        <td class="td">
		        <form method="POST" action="PogledajVM" ><input name="<%= kk.getIme() %>" type="submit" class="tabdugme" value="Pogledaj" ></form>
		        </td>
		    </tr>
		    <%}} %>
	    
		</table>
	</div>
	
	<div class="unos2">
		<h2>DISKOVI</h2>
		<table border="1" class="table">
		    <tr class="tr">
		        <th class="th">ID</th>
		        <th class="th">Ime</th>
		        <th class="th">Tip</th>
		        <th class="th">Kapacitet</th>
		        <th class="th">Virtuelna masina</th>
		        <th class="th"></th>
		    </tr>
		   <% indexx = 0; %>
		    <% for( Disk kk : Disk.ucitaj()) {if(kk.getOrg().equals(k.getOrganizacija())){%>
		    <tr <% if(indexx%2 == 0){%> class="tr" <% } else{ %> class="even" <%} %>>
		        <td class="td">
		            <%= ++indexx %>
		        </td>
		        <td class="td">
		            <%= kk.getIme() %>
		        </td>
		        <td class="td">
		            <%= kk.getTip() %>
		        </td>
		        <td class="td">
		        	<%= kk.getKapacitet() %>
		        </td>
		        <td class="td">
		        	<%= kk.getVm() %>
		        </td>

		        <td class="td">
		        <form method="POST" action="PogledajDisk" ><input name="<%= kk.getIme() %>" type="submit" class="tabdugme" value="Pogledaj" ></form>
		        </td>
		    </tr>
		    <%}} %>
	    
		</table>
	</div>
	<script>
		function tt() {
			var table, tr, td, td2, td3, i, txtValue, txtValue2, txtValue3;
			var odCore = document.getElementById("odCore").value;
			var doCore = document.getElementById("doCore").value;
			var odgcore = document.getElementById("odgcore").value;
			var dogcore = document.getElementById("dogcore").value;
			var odRAM = document.getElementById("odRAM").value;
			var doRAM = document.getElementById("doRAM").value;
			
			var core = false;
			var gcore = false;
			var ram = false;
			
			if(odCore == "" || doCore == ""){
				core = true;
			}
			if(odgcore == "" || dogcore == ""){
				gcore = true;
			}
			if(odRAM == "" || doRAM == ""){
				ram = true;
			}
			
			table = document.getElementById("tabb");
			tr = table.getElementsByTagName("tr");
			
			for (i = 0; i < tr.length; i++) {
				
		    	td = tr[i].getElementsByTagName("td")[4];// == undefined ? '' : tr[i].getElementsByTagName("td")[4].trim();
		    	td2 = tr[i].getElementsByTagName("td")[6];// == undefined ? '' : tr[i].getElementsByTagName("td")[6].trim();
		    	td3 = tr[i].getElementsByTagName("td")[5];// == undefined ? '' : tr[i].getElementsByTagName("td")[5].trim();
		    	
		    	if (td) {
		      		txtValue = td.textContent || td.innerText;
		      		txtValue2 = td2.textContent || td2.innerText;
		      		txtValue3 = td3.textContent || td3.innerText;
		      		
		      		txtValue = txtValue == undefined ? '' : txtValue.trim();
		      		txtValue2 = txtValue2 == undefined ? '' : txtValue2.trim();
		      		txtValue3 = txtValue3 == undefined ? '' : txtValue3.trim();
		      		
		      		var DAcore = parseInt(odCore) <= parseInt(txtValue) && parseInt(doCore) >= parseInt(txtValue)
		      		DAcore = DAcore || core;
		      		
		      		var DAgcore = parseInt(odgcore) <= parseInt(txtValue2) && parseInt(dogcore) >= parseInt(txtValue2)
		      		DAgcore = DAgcore || gcore;
		      		
		      		var DAram = parseFloat(odRAM) <= parseFloat(txtValue3) && parseFloat(doRAM) >= parseFloat(txtValue3)
		      		DAram = DAram || ram;
		      		
		      		if (DAcore && DAgcore && DAram) {
		        		tr[i].style.display = "";
		      		}
		      		else {
		        		tr[i].style.display = "none";
		      		}
		    	}       
		  	}
		}
	</script>