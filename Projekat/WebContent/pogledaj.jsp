<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="paket.Korisnik" %>
<%@page import="paket.Organizacija"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<% try{ %>
	<% Korisnik ko = (Korisnik)session.getAttribute("ulogovan"); %>
	<% Korisnik koga = new Korisnik(); %>
	<% String kogastr = (String)request.getSession().getAttribute("koga"); %>
	<% for(Korisnik kkkk : Korisnik.ucitajKorisnike()) {%>
	<% 		System.out.println(kkkk.getEmail()); System.out.println(" == "); System.out.println(kogastr);%>
	<% 		if(kkkk.getEmail().equals(kogastr)){%>
	<% 			koga = kkkk;%>
	<% } 	}System.out.println("\n\n\n\n");System.out.println(koga.getEmail());%>
	<%// if(koga.getEmail().equals("")){ response.sendError(403); }%>
	<head>
		<title><%= koga.getIme() %> <%= koga.getPrezime() %></title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body>
		<%@ include file="strana/glava.jsp" %>
		<div class="unos">
			<h1><%= koga.getIme() %> <%= koga.getPrezime() %></h1><br />
			<% request.setAttribute("koga", kogastr); %>
			<% if(!ko.getEmail().equals(koga.getEmail())) {%><!-- da li moze da obrise sebe????? -->
			<form method="POST" action="ObrisiKorisnika" onSubmit="return proveri();"><input type="submit" class="sub" value="Obrisi" ></form>
			<% } %>
		</div>
		<form method="POST" action="IzmeniKorisnika" onSubmit="return proveri();" >
			<div class="unos2">
				<table>
					<tr>
						<th align="right">Ime </th>
						<th><input type="text" name="ime" id="ime" value="<%= koga.getIme() %>"></th>
						<th></th>
					</tr>
					<tr>
						<th align="right">Prezime </th>
						<th><input type="text" name="prezime" id="prezime" value="<%= koga.getPrezime() %>"></th>
						<th></th>
					</tr>
					<tr>
						<th align="right">Email </th>
						<th><input type="text" name="email" id="email" <% if(!ko.getEmail().equals(koga.getEmail())) {%> disabled <% } %> value="<%= koga.getEmail() %>"></th>
						<th align="left"><label id="lemail" style="color: red;"></label></th>
					</tr>
					<tr>
						<th align="right">Password </th><!-- Zar moze da vidi njegovu lozinku????? -->
						<th><input type="text" name="password" id="password" value="<%= koga.getPassword() %>"></th>
						<th align="left"><label id="lpassword" style="color: red;"></label></th>
					</tr>
					<% if(ko.getEmail().equals(koga.getEmail())) {%> 
					
					<tr>
						<th align="right">Ponovite Password </th><!-- Zar moze da vidi njegovu lozinku????? -->
						<th><input type="text" name="password2" id="password2" value="<%= koga.getPassword() %>"></th>
						<th align="left"><label id="lpassword2" style="color: red;"></label></th>
					</tr>
					
					<% } %>
					<% if(ko.getEmail().equals(koga.getEmail()) && !ko.getUloga().equals("sa")){ %>
					<tr>
						<th align="right">Organizacija </th>
						
						<th align="left"><select name="organizacija" id="organizacija">
						<% for( Organizacija org : Organizacija.ucitaj()) {%>
							<option value="<%= org.getIme() %>"><%= org.getIme() %></option>
						<%} %>
						</select></th>
						<th align="left"><label id="lorganizacija" style="color: red;"></label></th>
					</tr>
					<% }else if(koga.getUloga().equals("sa")) { %>

					<% } else{ %>
					<tr>
						<th align="right">Organizacija </th>
						<th><input type="text" name="organizacija" id="organizacija" disabled value="<%= koga.getOrganizacija() %>"></th>
						<th></th>
					</tr>
					<% } %>
					<tr>
						<th></th>
						<th align="left"><input type="submit" class="sub" value="Sacuvaj Promene"></th>
						<th align="left"><label style="color: red;">
							<% if(request.getAttribute("poruka")!=null){ %>
							<%= request.getAttribute("poruka") %>
							<% }%>
						</label></th>
					</tr>
				</table>
			</div>
		</form>
		<script>
			function proveri() {
				var popunjeno = true;
				
				if(document.getElementById("email").value == ""){
					popunjeno = false
					document.getElementById('lemail').innerHTML = 'Ovo polje je obavezno!';
				}
				else if(document.getElementById("email").value.includes("@") == false || document.getElementById("email").value.includes(".") == false){
					popunjeno = false
					document.getElementById('lemail').innerHTML = 'Email koji ste uneli je pogresan!';
				}
				else{
					document.getElementById('lemail').innerHTML = '';
				}
				
				if(document.getElementById("password").value == ""){
					popunjeno = false
					document.getElementById('lpassword').innerHTML = 'Ovo polje je obavezno!';
				}
				else if(document.getElementById("password").value.length < 8){
					popunjeno = false
					document.getElementById('lpassword').innerHTML = 'Lozinka mora da sadrzi minimalno 8 karaktera!';
				}
				else{
					document.getElementById('lpassword').innerHTML = '';
				}
				<% if(ko.getEmail().equals(koga.getEmail())) {%> 
				if(document.getElementById("password").value != document.getElementById("password2").value){
					popunjeno = false
					document.getElementById('lpassword2').innerHTML = 'Lozinke se ne poklapaju';
				}
				else{
					document.getElementById('lpassword2').innerHTML = '';
				}
				<% } %> 
				if(!popunjeno) {alert("Morate da pravilno popunite sva polja")}
				return popunjeno;
			}
		</script>
	</body>
<% } catch(Exception e){ response.sendError(403); } %>
</html>