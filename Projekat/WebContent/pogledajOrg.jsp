<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="paket.Korisnik" %>
<%@ page import="paket.Organizacija" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<% try{ %>
	<% Korisnik ko = (Korisnik)session.getAttribute("ulogovan"); %>
	<% Organizacija koga = new Organizacija(); %>
	<% String kogastr = (String)request.getSession().getAttribute("koga"); %>
	<% for(Organizacija kkkk : Organizacija.ucitaj()) {%>
	<% 		if(kkkk.getIme().equals(kogastr)){%>
	<% 			koga = kkkk;%>
	<% } 	}%>
	<head>
		<title><%= koga.getIme() %></title>
		<link rel="stylesheet" href="css/stil.css">
	</head>
	<body>
		<%@ include file="strana/glava.jsp" %>
		<div class="unos">
			<h1><%= koga.getIme() %></h1><br />
			<% request.setAttribute("koga", kogastr); %>
			<% if(ko.getUloga().equals("sa") && false) {%><form method="POST" action="ObrisiOrganizaciju" ><input type="submit" class="sub" value="Obrisi" ></form><%} %>
		</div>
		<form method="POST" action="IzmeniOrganizaciju" >
			<div class="unos2">
				<table>
					<tr>
						<th align="right">Ime </th>
						<th><input type="text" name="ime" id="ime" value="<%= koga.getIme() %>" <% if(ko.getUloga().equals("k")) {%>disabled<%} %>></th>
					</tr>
					<tr>
						<th align="right">Opis </th>
						<% if(!ko.getUloga().equals("k")) {%>
						<th><input type="text" name="opis" id="opis" value="<%= koga.getOpis() %>" <% if(ko.getUloga().equals("k")) {%>disabled<%} %>></th>
						<%} %>
					</tr>
					<tr>
					<% if(!ko.getUloga().equals("k")) {%>
						<th align="right">Logo </th>
						<th><input type="file" name="logo" id="logo" accept="image/*" <% if(ko.getUloga().equals("k")) {%>disabled<%} %>></th>
					<%} %>
					</tr>
					<tr>
						<th></th>
						<% if(!ko.getUloga().equals("k")) {%>
							<th align="left"><input type="submit" class="sub" value="Sacuvaj Promene"></th>
						<%} else{%>
							<th align="left"><%= koga.getOpis() %></th>
						<%} %>
					</tr>
				</table>
			</div>
		</form>
	</body>
<% } catch(Exception e){ response.sendError(403); } %>
</html>