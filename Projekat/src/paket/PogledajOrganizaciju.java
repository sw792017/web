package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class PogledajOrganizaciju
 */
@WebServlet("/PogledajOrganizaciju")
public class PogledajOrganizaciju extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PogledajOrganizaciju() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Korisnik k = (Korisnik)session.getAttribute("ulogovan");
			ArrayList<Organizacija> kor = Organizacija.ucitaj();
			System.out.println(k.toString());
			Organizacija kk = new Organizacija();
			for(Organizacija kkk : kor) {
				if (request.getParameter(kkk.getIme()) != null){
				       kk = kkk;
				       break;
				 }
			}
			System.out.println(kk.toString());
				if( k.getUloga().equals("sa") || kk.getIme().equals(k.getOrganizacija() )) {
					request.getSession().setAttribute("koga", kk.getIme());
					response.sendRedirect("pogledajOrg.jsp");
					return;
				}
				else {
					response.sendError(403);
					return;
				}
				
		}catch (Exception e) {
			response.sendError(400);
			return;
		}
	}
}
