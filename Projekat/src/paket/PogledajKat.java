package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class PogledajKat
 */
@WebServlet("/PogledajKat")
public class PogledajKat extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PogledajKat() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Korisnik k = (Korisnik)session.getAttribute("ulogovan");
			ArrayList<Kategorija> kor = Kategorija.ucitaj();
			System.out.println(k.toString());
			Kategorija kk = new Kategorija("", 0, 0);
			for(Kategorija kkk : kor) {
				if (request.getParameter(kkk.getIme()) != null){
				       kk = kkk;
				       break;
				 }
			}
			System.out.println(kk.toString());
			if(kk.getIme().equals("")) {
				response.sendError(400);
				return;
			}
				if( k.getUloga().equals("sa") || (k.getUloga().equals("a"))) {
					request.getSession().setAttribute("koga", kk.getIme());
					response.sendRedirect("pogledajKategoriju.jsp");
					return;
				}
				else {
					response.sendError(403);
					return;
				}
				
		}catch (Exception e) {
			response.sendError(403);
			return;
		}
	}
}
