package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class IzmeniKorisnika
 */
@WebServlet("/IzmeniKorisnika")
public class IzmeniKorisnika extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IzmeniKorisnika() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			boolean ponadjen = false;
			String page = "pocetna.jsp";
			HttpSession session = request.getSession();
			Korisnik korisnik = (Korisnik)session.getAttribute("ulogovan");
			System.out.println(korisnik.toString());
			Korisnik kk = new Korisnik();
			
			String kogastr = (String)request.getSession().getAttribute("koga"); 
			for(Korisnik kkkk : Korisnik.ucitajKorisnike()) {
			 	System.out.println(kkkk.getEmail()); System.out.println(" == "); System.out.println(kogastr);
			 	if(kkkk.getEmail().equals(kogastr)){
					kk = kkkk;
					ponadjen = true;
				    break;
			 	}
			}
			
			ArrayList<Korisnik> lista = Korisnik.ucitajKorisnike();

			lista.remove(kk);
			
			if(request.getParameter("password").length() > 7)
				kk.setPassword(request.getParameter("password"));
			
			if(request.getParameter("ime") != "")
				kk.setIme(request.getParameter("ime"));
			
			if(request.getParameter("prezime") != "")
				kk.setPrezime(request.getParameter("prezime"));
			
			if(korisnik.getEmail().equals(kk.getEmail()) && request.getParameter("email").contains("@") && request.getParameter("email").contains(".")) {
				boolean postoji = false;
				for(Korisnik kkkk : Korisnik.ucitajKorisnike()) {
					if (kkkk.getEmail().equals(request.getParameter("email")))
						postoji = true;
				}
				if(!postoji)
					kk.setEmail(request.getParameter("email"));
			}
			
			/*if(request.getParameter("organizacija") != "" && korisnik.getEmail().equals(kk.getEmail()) && !korisnik.getUloga().equals("sa"))
				kk.setOrganizacija(request.getParameter("organizacija"));*///niko nema pravo da ovo menja

			
			lista.add(kk);
			
			if(ponadjen) {
				if(korisnik.getUloga().equals("sa") || (korisnik.getUloga().equals("a") && kk.getOrganizacija().equals(korisnik.getOrganizacija()) || korisnik.getEmail().contentEquals(kk.getEmail())  )) {
					Korisnik.upisiKorisnika(lista);
					
					RequestDispatcher dd=request.getRequestDispatcher(page);
					dd.forward(request, response);
					return;
				}
				else {
					response.sendError(403);
					return;
				}
			}
			else {
				response.sendError(400);
				return;
			}

		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}

}
