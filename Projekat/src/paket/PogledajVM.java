package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class PogledajVM
 */
@WebServlet("/PogledajVM")
public class PogledajVM extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PogledajVM() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Korisnik k = (Korisnik)session.getAttribute("ulogovan");
			request.setAttribute("porukaa", "");
			ArrayList<VirtualnaMasina> vms = VirtualnaMasina.ucitaj();
			VirtualnaMasina vm = new VirtualnaMasina("", "", new Kategorija("", 0, 0));
			for(VirtualnaMasina kkk : vms) {
				if (request.getParameter(kkk.getIme()) != null){
				       vm = kkk;
				       break;
				 }
			}
			if(vm.getIme() == "") {
				response.sendError(400);
				return;
			}

			if( k.getUloga().equals("sa") || vm.getOrganizacija().equals(k.getOrganizacija())) {
				request.getSession().setAttribute("koga", vm.getIme());
				response.sendRedirect("pogledajVM.jsp");
				return;
			}
			else {
				response.sendError(403);
				return;
			}
				
		}catch (Exception e) {
			response.sendError(400);
			return;
		}
	}

}
