package paket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class Kategorija {
	private String ime;
	private int brJezgra;
	private double ram;
	private int brGPUJezgra;
	
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public int getBrJezgra() {
		return brJezgra;
	}
	public void setBrJezgra(int brJezgra) {
		this.brJezgra = brJezgra;
	}
	public double getRam() {
		return ram;
	}
	public void setRam(double ram) {
		this.ram = ram;
	}
	public int getBrGPUJezgra() {
		return brGPUJezgra;
	}
	public void setBrGPUJezgra(int brGPUJezgra) {
		this.brGPUJezgra = brGPUJezgra;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
	    if (obj == this) return true;
	    if (!(obj instanceof Kategorija)) return false;
	    Kategorija o = (Kategorija) obj;
	    return o.getIme().equals(this.getIme());
	}
	@Override
	public String toString() {
		String s = "\n\t{\n\t\t\"ime\": \"" + this.ime + "\",\n" +
				"\t\t\"brJezgra\": \"" + this.brJezgra + "\",\n" +
				"\t\t\"ram\": \"" + this.ram + "\",\n" +
				"\t\t\"brGPUJezgra\": \"" + this.brGPUJezgra + "\",\n" +
				"\t},";
		return s;
	}
	public Kategorija(String ime, int brJezgra, double ram, int brGPUJezgra) {
		this.ime = ime;
		this.brJezgra = brJezgra;
		if (brJezgra < 1)
			this.brJezgra = 1;
		this.ram = ram;
		if (ram < 1)
			this.ram = 1;
		this.brGPUJezgra = brGPUJezgra;
		if (brGPUJezgra < 0)
			this.brGPUJezgra = 0;
	}
	
	public Kategorija(String ime, int brJezgra, double ram) {
		this.ime = ime;
		this.brJezgra = brJezgra;
		if (brJezgra < 1)
			this.brJezgra = 1;
		this.ram = ram;
		if (ram < 1)
			this.ram = 1;
		this.brGPUJezgra = 0;
	}
	
	public Kategorija(String s) {
		s = s.replace('\n', ' ');
		String[] ss = s.split("\"");
		this.ime = ss[3];
		this.brJezgra = Integer.valueOf(ss[7]);
		this.ram = Double.valueOf(ss[11]);
		this.brGPUJezgra = Integer.valueOf(ss[15]);
	}

	public static boolean upisi(ArrayList<Kategorija> lista) {
		try {
			File fajl = new File("kategorije.json");
			fajl.delete();
			fajl.createNewFile();
			BufferedWriter writer = new BufferedWriter(new FileWriter(fajl));
			String json = "{\n\t\"Kategorija\": [";
			for(Kategorija k : lista) {
				json += k.toString();
			}
			json = json.substring(0, json.length()-1);
			json += "\n\t]\n}";
			writer.write(json);
			writer.close();
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public static boolean napraviFajl() {
		ArrayList<Kategorija> lista = new ArrayList<Kategorija>();
		return upisi(lista);
	}
	
	public static ArrayList<Kategorija> ucitaj() {
		
		ArrayList<Kategorija> lista = new ArrayList<Kategorija>();
		
		try {
			File fajl = new File("kategorije.json");
			if(!fajl.exists() || fajl.length() == 0) {
				napraviFajl(); // ako ne postoji
			}
			
			BufferedReader reader = new BufferedReader(new FileReader("kategorije.json"));
			String line = reader.readLine();
			String element = "";
			boolean pisi = false;
			while (line != null) {
				if(line.contains("\"ime\": \""))
					pisi = true;
				if(pisi)
					element += line;
				if(line.contains("\"brGPUJezgra\": \"")) {
					pisi = false;
					lista.add(new Kategorija(element));
					element = "";
				}
				
				line = reader.readLine();
			}
			reader.close();
			return lista;
		}
		catch (Exception e) {
			return lista;
		}
	}
}
