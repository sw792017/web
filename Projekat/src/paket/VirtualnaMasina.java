package paket;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class VirtualnaMasina {
	private String ime;//jedinstveno
	private String organizacija;
	private Kategorija kategorija;
	private int brJezgra;
	private double ram;
	private int brGPUJezgra;
	private String[] diskovi;
	private String[] aktivnosti;

	@Override
	public String toString() {
		for(String stt : this.diskovi)
			stt = "\"" + stt + "\"";
		for(String stt : this.aktivnosti)
			stt = "\"" + stt + "\"";
		
		String akt = Arrays.toString(this.aktivnosti);
		if(akt.contains("[, "))
			akt = akt.replace("[, ", "[");
		
		String s = "\n\t{\n\t\t\"ime\": \"" + this.ime + "\",\n" +
				"\t\t\"organizacija\": \"" + this.organizacija + "\",\n" +
				"\t\t\"kategorija\": \"" + this.kategorija.getIme() + "\",\n" +
				"\t\t\"diskovi\": " + Arrays.toString(this.diskovi) + ",\n" +
				"\t\t\"resursi\": " + akt + "\n" +
				"\t},";
		return s;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (obj == null) return false;
	    if (obj == this) return true;
	    if (!(obj instanceof VirtualnaMasina)) return false;
	    VirtualnaMasina o = (VirtualnaMasina) obj;
	    return o.getIme().equals(this.getIme());
	}
	
	public static String[] vratiPoKategorijiIme(int i) {
		ArrayList<String> odgovor = new ArrayList<String>();
		ArrayList<Kategorija> kategorije = Kategorija.ucitaj();
		for(VirtualnaMasina v : VirtualnaMasina.ucitaj()) {
			if(v.getKategorija().equals(kategorije.get(i))) {
				odgovor.add(v.getIme());
			}
		}
		return (String[]) odgovor.toArray();
	}
	
	public static String[] vratiPoKategorijiBrJezgra(int i) {
		ArrayList<String> odgovor = new ArrayList<String>();
		ArrayList<Kategorija> kategorije = Kategorija.ucitaj();
		for(VirtualnaMasina v : VirtualnaMasina.ucitaj()) {
			if(v.getKategorija().equals(kategorije.get(i))) {
				odgovor.add(Integer.toString(v.getBrJezgra()));
			}
		}
		return (String[]) odgovor.toArray();
	}
	
	public static String[] vratiPoKategorijiBrGPUJezgra(int i) {
		ArrayList<String> odgovor = new ArrayList<String>();
		ArrayList<Kategorija> kategorije = Kategorija.ucitaj();
		for(VirtualnaMasina v : VirtualnaMasina.ucitaj()) {
			if(v.getKategorija().equals(kategorije.get(i))) {
				odgovor.add(Integer.toString(v.getBrGPUJezgra()));
			}
		}
		return (String[]) odgovor.toArray();
	}
	
	public static String[] vratiPoKategorijiRAM(int i) {
		ArrayList<String> odgovor = new ArrayList<String>();
		ArrayList<Kategorija> kategorije = Kategorija.ucitaj();
		for(VirtualnaMasina v : VirtualnaMasina.ucitaj()) {
			if(v.getKategorija().equals(kategorije.get(i))) {
				odgovor.add(Double.toString(v.getRam()));
			}
		}
		return (String[]) odgovor.toArray();
	}
	
	public static String vratiDiskove() {
		/*
		ArrayList<String> odgovor = new ArrayList<String>();
		
		for(Organizacija o : Organizacija.ucitaj()) {
			String s = "";
			for(Disk d : Disk.ucitaj()) {
				if(o.getIme().equals(d.getOrg())) {
					String ime = d.getIme();
					String tekst = d.getIme();
					
					s += ("<input type=\"checkbox\" id=\"" + ime + "\" name=\"" + ime + "\" value=\"" + ime  + "\">" + 
							"  <label for=\"" + ime  + "\"> " + tekst + "</label><br />");
				}
			}
			odgovor.add(s);
		}
		
		String stt = "[";
		
		for(int i = 0 ; i < odgovor.size() ; ++i) {
			stt += " '" + odgovor.get(i) + "' ";
			if(odgovor.size()-i != 1)
				stt += ", ";
		}
		
			

		return stt + "]";//(String[]) odgovor.toArray();
		*/ //&& (d.getVm().equals(ime) || d.getVm().equals("") || d.getVm().equals(null))
		ArrayList<String> odgovor = new ArrayList<String>();
		
		for(Organizacija o : Organizacija.ucitaj()) {
			String s = "";
			for(Disk d : Disk.ucitaj()) {
				if(o.getIme().equals(d.getOrg()) && (d.getVm().equals("") || d.getVm().equals(null))) {
					String ime = d.getIme();
					String tekst = d.getIme();
					s += ("<input type=\"checkbox\" id=\"" + ime + "\" name=\"" + ime + "\" value=\"" + ime  + "\">" + 
							"  <label for=\"" + ime  + "\"> " + tekst + "</label><br />");
				}
			}
			odgovor.add(s);
		}
		String stt = "[";
		
		for(int i = 0 ; i < odgovor.size() ; ++i) {
			stt += " '" + odgovor.get(i) + "' ";
			if(odgovor.size()-i != 1)
				stt += ", ";
		}

		return stt + "]";//(String[]) odgovor.toArray();
	}
	
	public static String vratiVeze(String s) {
		/*String odgovor = "<select id=\"VM\" name=\"VM\">" + 
				"<option value=\"" + s + "\">" + s + "</option>;";
		
		for(VirtualnaMasina v : VirtualnaMasina.ucitaj())
			if(!v.getIme().equals(s))
				odgovor += "<option value=\"" + v.getIme() + "\">" + v.getIme() + "</option>";

		return odgovor + "</select>";*/
		String NAZIV_POLJA = "VMasina";
		
		String odgovor =  "<input type=\"radio\" id=\"" + "nikome" + "\" name=\"" + NAZIV_POLJA + "\" value=\"\" checked>" + 
				" <label for=\"" + "nikome" + "\">" + "<b><i>[Ni jednom]</i></b>" + "</label><br>";
		
		Disk disk = new Disk("", "", "");
		
		for(Disk d : Disk.ucitaj()) {
			if (d.getIme().equals(s)) {
				disk = d;
				break;
			}
		}

		for(VirtualnaMasina v : VirtualnaMasina.ucitaj()) {
			if(v.getOrganizacija().equals(disk.getOrg())) {
				String ime = v.getIme();
				if(disk.getVm().equals(v.getIme())) {
					odgovor += "<input type=\"radio\" id=\"" + ime + "\" name=\"" + NAZIV_POLJA + "\" value=\"" + ime + "\" checked>" + 
							" <label for=\"" + ime + "\">" + ime + "</label><br>";
				}else {
					odgovor += "<input type=\"radio\" id=\"" + ime + "\" name=\"" + NAZIV_POLJA + "\" value=\"" + ime + "\">" + 
							" <label for=\"" + ime + "\">" + ime + "</label><br>";
				}
			}
		}
		return odgovor;
	}

	//////////////////////////////////////////////////
	
	/*public static String vratiPoKategorijiIme() {
		ArrayList<String[]> odgovor = new ArrayList<String[]>();
		//ArrayList<Kategorija> kategorije = Kategorija.ucitaj();
		for(Kategorija kategorije : Kategorija.ucitaj()) {
			ArrayList<String> odg = new ArrayList<String>();
			for(VirtualnaMasina v : VirtualnaMasina.ucitaj()) {
				if(v.getKategorija().equals(kategorije)) {
					odg.add(v.getIme());
				}
			}
			odgovor.add((String[]) odg.toArray());
		}
		return odgovor.toArray().toString();
	}*/ // strari kod
	
	public static String vratiPoKategorijiBrJezgra() {
		/*ArrayList<String> odgovor = new ArrayList<String>();
		for(Kategorija kategorije : Kategorija.ucitaj()) {
			ArrayList<String> odg = new ArrayList<String>();
			for(VirtualnaMasina v : VirtualnaMasina.ucitaj()) {
				if(v.getKategorija().equals(kategorije)) {
					odg.add(Integer.toString(v.getBrJezgra()));
				}
			}
			odgovor.add(Arrays.toString((String[]) odg.toArray()));
		}
		return Arrays.toString(odgovor.toArray());*/
		
		/*String odgovor = "\nBrJezgra = [";
		int index = 0;
		for(Kategorija kategorije : Kategorija.ucitaj()) {
			ArrayList<String> odg = new ArrayList<String>();
			for(VirtualnaMasina v : VirtualnaMasina.ucitaj()) {
				if(v.getKategorija().equals(kategorije)) {
					odg.add(Integer.toString(v.getBrJezgra()));
				}
			}
			odgovor += (Arrays.toString(odg.toArray()));
			if(index++ == Kategorija.ucitaj().size()) {
				odgovor += ",\n";
			}
		}
		
		return odgovor;*/
		
		String odgovor = "[";
		int index = 0;
		//System.out.println("Kategorija : " + Kategorija.ucitaj().size());
		for(Kategorija kategorije : Kategorija.ucitaj()) {
			//System.out.println("Usao : " + (index+1));
			odgovor += kategorije.getBrJezgra();
			if(++index < Kategorija.ucitaj().size()) {
				odgovor += ", ";
			}
		}
		
		return odgovor + "]";
	}
	
	public static String vratiPoKategorijiBrGPUJezgra() {
		/*ArrayList<String> odgovor = new ArrayList<String>();
		System.out.println("Kategorija : " + Kategorija.ucitaj().size());
		for(Kategorija kategorije : Kategorija.ucitaj()) {
			ArrayList<String> odg = new ArrayList<String>();
			System.out.println("VMa : " + VirtualnaMasina.ucitaj().size());
			for(VirtualnaMasina v : VirtualnaMasina.ucitaj()) {
				System.out.println(v.getKategorija().getIme() +" ?= " + kategorije.getIme() + " => " + v.getKategorija().equals(kategorije));
				if(v.getKategorija().equals(kategorije)) {
					odg.add(Integer.toString(v.getBrGPUJezgra()));
				}
			}
			System.out.println(Arrays.toString((String[]) odg.toArray()));
			odgovor.add(Arrays.toString((String[]) odg.toArray()));
		}
		System.out.println(Arrays.toString(odgovor.toArray()));
		return Arrays.toString(odgovor.toArray());*/
		
		String odgovor = "[";
		int index = 0;
		for(Kategorija kategorije : Kategorija.ucitaj()) {
			odgovor += kategorije.getBrGPUJezgra();
			if(++index < Kategorija.ucitaj().size()) {
				odgovor += ", ";
			}
		}
		
		return odgovor + "]";
	}
	
	public static String vratiPoKategorijiRAM() {
		/*ArrayList<String> odgovor = new ArrayList<String>();
		for(Kategorija kategorije : Kategorija.ucitaj()) {
			ArrayList<String> odg = new ArrayList<String>();
			for(VirtualnaMasina v : VirtualnaMasina.ucitaj()) {
				if(v.getKategorija().equals(kategorije)) {
					odg.add(Double.toString(v.getRam()));
				}
			}
			odgovor.add(Arrays.toString((String[]) odg.toArray()));
		}
		return Arrays.toString(odgovor.toArray());*/
		String odgovor = "[";
		int index = 0;
		for(Kategorija kategorije : Kategorija.ucitaj()) {
			odgovor += kategorije.getRam();
			if(++index < Kategorija.ucitaj().size()) {
				odgovor += ", ";
			}
		}
		
		return odgovor + "]";
	}

	public static String vratiOznaceneDiskove(String ime) {
		/*
		String odg = "";
		
		VirtualnaMasina vm = new VirtualnaMasina("", "", new Kategorija("", 0, 0));
		
		for(VirtualnaMasina vm2 : VirtualnaMasina.ucitaj()) {
			if(vm2.getIme().equals(ime))
				vm = vm2;
		}
		
		if(vm.getIme() == "")
			return "";
		
		for(Disk d : Disk.ucitaj()) {
			if(d.getOrg().equals(vm.getOrganizacija())) {
				List<String> lista = Arrays.asList(vm.getDiskovi());
				String tekst = d.getIme();
				if(lista.contains(d.getIme()))
					odg += ("<input type=\"checkbox\" id=\"" + tekst + "\" name=\"" + tekst + "\" value=\"" + tekst  + "\" checked>" + 
							"  <label for=\"" + tekst  + "\"> " + tekst + "</label><br />");
				else
					odg += ("<input type=\"checkbox\" id=\"" + tekst + "\" name=\"" + tekst + "\" value=\"" + tekst  + "\">" + 
							"  <label for=\"" + tekst  + "\"> " + tekst + "</label><br />");
			}
		}
		return odg;
		*/
		String odg = "";
		
		VirtualnaMasina vm = new VirtualnaMasina("", "", new Kategorija("", 0, 0));
		
		for(VirtualnaMasina vm2 : VirtualnaMasina.ucitaj()) {
			if(vm2.getIme().equals(ime))
				vm = vm2;
		}
		
		if(vm.getIme() == "")
			return "";
		
		for(Disk d : Disk.ucitaj()) {
			if(d.getOrg().equals(vm.getOrganizacija()) && (d.getVm().equals(ime) || d.getVm().equals("") || d.getVm().equals(null))) {
				List<String> lista = Arrays.asList(vm.getDiskovi());
				String tekst = d.getIme();
				if(lista.contains(d.getIme()))
					odg += ("<input type=\"checkbox\" id=\"" + tekst + "\" name=\"" + tekst + "\" value=\"" + tekst  + "\" checked>" + 
							"  <label for=\"" + tekst  + "\"> " + tekst + "</label><br />");
				else
					odg += ("<input type=\"checkbox\" id=\"" + tekst + "\" name=\"" + tekst + "\" value=\"" + tekst  + "\">" + 
							"  <label for=\"" + tekst  + "\"> " + tekst + "</label><br />");
			}
		}
		return odg;
	}
	
	public VirtualnaMasina(String ime, String organizacija, Kategorija kategorija, String[] diskovi, String[] aktivnosti) {
		this.ime = ime;
		this.organizacija = organizacija;
		this.kategorija = kategorija;
		this.brJezgra = kategorija.getBrJezgra();
		this.ram = kategorija.getRam();
		this.brGPUJezgra = kategorija.getBrGPUJezgra();
		this.diskovi = diskovi;
		this.aktivnosti = aktivnosti;
	}
	
	public VirtualnaMasina(String ime, String organizacija, Kategorija kategorija) {
		this.ime = ime;
		this.organizacija = organizacija;
		this.kategorija = kategorija;
		this.brJezgra = kategorija.getBrJezgra();
		this.ram = kategorija.getRam();
		this.brGPUJezgra = kategorija.getBrGPUJezgra();
		this.diskovi = new String[0];
		this.aktivnosti = new String[0];
	}
	
	public VirtualnaMasina(String s) {
		s = s.replace('\n', ' ');
		String[] sss = s.split("\\[");
		String[] ss = sss[0].split("\"");
		this.ime = ss[3];
		this.organizacija = ss[7];
		Kategorija kat = new Kategorija("Greska", 0, 0.0);
		
		for(Kategorija kat1 : Kategorija.ucitaj())
			if(kat1.getIme().equals(ss[11]))
				kat = kat1;
		this.kategorija = kat;
		
		this.brJezgra = kat.getBrJezgra();
		this.ram = kat.getRam();
		this.brGPUJezgra = kat.getBrGPUJezgra();
		
		this.diskovi = new String[0];
		this.aktivnosti = new String[0];
		/*
		System.out.println("-------------------------------");
		System.out.print("ime ");
		System.out.println(this.ime);
		System.out.print("organizacija ");
		System.out.println(this.organizacija);
		System.out.print("kategorija ");
		System.out.println(this.kategorija);
		System.out.println("-------------------------------");
		*/
		ArrayList<String> ar = new ArrayList<String>();
		
		try {
			String[] ssss = sss[1].split("\\]")[0].split("\"");// "a", "b", "c"
			ssss = ssss[0].split(",");
			
			for(int i = 0 ; i < ssss.length ; ++i)
				ar.add(ssss[i].trim());
			
			String nizovi[] = new String[ar.size()];
			for(int i = 0 ; i < ar.size() ; ++i)
				nizovi[i] = ar.get(i);
			
			this.diskovi = nizovi;//(String[])ar.toArray();
		}catch (Exception e) {}
		try {
			sss[2] = " " + sss[2];// ako je prazan
			ar = new ArrayList<String>();
			
			String[] ssss2 = sss[2].split("\\]")[0].split("\"");// "a", "b", "c"
			ssss2 = ssss2[0].split(",");
			
			for(int i = 0 ; i < ssss2.length ; ++i)
				ar.add(ssss2[i].trim());
			
			////
			String nizovi[] = new String[ar.size()];
			for(int i = 0 ; i < ar.size() ; ++i)
				nizovi[i] = ar.get(i);
			
			this.aktivnosti = nizovi;
			////
			
		}catch (Exception e) { }
	}
	
	public static boolean upisi(ArrayList<VirtualnaMasina> lista) {
		try {
			File fajl = new File("virtualnemasine.json");
			fajl.delete();
			fajl.createNewFile();
			BufferedWriter writer = new BufferedWriter(new FileWriter(fajl));
			String json = "{\n\t\"VirtualnaMasina\": [";
			for(VirtualnaMasina k : lista) {
				json += k.toString();
			}
			json = json.substring(0, json.length()-1);
			json += "\n\t]\n}";
			writer.write(json);
			writer.close();
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public static boolean napraviFajl() {
		ArrayList<VirtualnaMasina> lista = new ArrayList<VirtualnaMasina>();
		return upisi(lista);
	}
	
	public static ArrayList<VirtualnaMasina> ucitaj() {
		ArrayList<VirtualnaMasina> lista = new ArrayList<VirtualnaMasina>();
		
		try {
			File fajl = new File("virtualnemasine.json");
			if(!fajl.exists() || fajl.length() == 0) {
				napraviFajl(); // ako ne postoji, napravi 3
			}
			//System.out.println("----------------------OTVARA FAJL");
			BufferedReader reader = new BufferedReader(new FileReader("virtualnemasine.json"));
			String line = reader.readLine();
			//System.out.println("sve - " + line);
			String element = "";
			boolean pisi = false;
			while (line != null) {
				//System.out.println("linija - " + line);
				if(line.contains("\"ime\": \""))
					pisi = true;
				if(pisi)
					element += line;
				if(line.contains("\"resursi\": [")) {
					//System.out.println("-----------------poziva konvertor");
					pisi = false;
					lista.add(new VirtualnaMasina(element));
					element = "";
				}
				line = reader.readLine();
			}
			reader.close();
			return lista;
		}
		catch (Exception e) {
			return lista;
		}
	}
	
	public void addDisk(String disk) {
		if(this.diskovi == null)
			this.diskovi = new String[0];
		ArrayList<String> lista = new ArrayList<String>();
		
		try {
			for(String s : this.diskovi) {
				lista.add(s);
			}
		}catch (Exception e) { }
		
		lista.add(disk);
		
		String[] ss = new String[lista.size()];
		
		for(int i = 0 ; i < lista.size() ; ++i) {
			ss[i] = lista.get(i);
		}
		
		this.diskovi = ss;
		/*System.out.println("upisuje "+disk);
		System.out.println(this.diskovi);
		System.out.println("su null ? " + (this.diskovi == null));
		
		if(this.diskovi == null)
			this.diskovi = new String[0];
		
		System.out.println("nije if");
		List<String> lista = Arrays.asList(this.diskovi);
		System.out.println("konvertovao");
		lista.add(disk);
		System.out.println("dodao");
		this.diskovi = (String[]) lista.toArray();
		System.out.println("vratio");*/
		/*String addd[] = new String[this.diskovi.length + 1]; 
        for (int i = 0; i < this.diskovi.length + 1; i++) 
            addd[i] = this.diskovi[i];
  
        addd[this.diskovi.length+1] = disk;
        this.setDiskovi(addd);*/
	}
	
	public void addAktivnost(LocalDateTime poc) {
		if(this.aktivnosti == null)
			this.aktivnosti = new String[0];
		
		try {
			if(this.getAktivnosti()[0] == "" || this.getAktivnosti()[0] == " ") {
				this.aktivnosti = new String[0];
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		ArrayList<String> lista = new ArrayList<String>();
		
		try {
			for(String s : this.aktivnosti) {
				lista.add(s);
			}
		}catch (Exception e) { }
		
		lista.add(poc.toString());
		
		String[] ss = new String[lista.size()];
		
		for(int i = 0 ; i < lista.size() ; ++i) {
			ss[i] = lista.get(i);
		}
		
		this.aktivnosti = ss;
		/*String addd[] = new String[this.aktivnosti.length + 1]; 
        for (int i = 0; i < this.aktivnosti.length + 1; i++) 
            addd[i] = this.aktivnosti[i];
  
        addd[this.aktivnosti.length+1] = poc.toString() + "-" + LocalDateTime.now().toString();
        this.setAktivnosti(addd);*/
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getOrganizacija() {
		return organizacija;
	}

	public void setOrganizacija(String organizacija) {
		this.organizacija = organizacija;
	}

	public Kategorija getKategorija() {
		return kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}

	public int getBrJezgra() {
		return brJezgra;
	}

	public void setBrJezgra(int brJezgra) {
		this.brJezgra = brJezgra;
	}

	public double getRam() {
		return ram;
	}

	public void setRam(double ram) {
		this.ram = ram;
	}

	public int getBrGPUJezgra() {
		return brGPUJezgra;
	}

	public void setBrGPUJezgra(int brGPUJezgra) {
		this.brGPUJezgra = brGPUJezgra;
	}

	public String[] getDiskovi() {
		return diskovi;
	}

	public void setDiskovi(String[] diskovi) {
		this.diskovi = diskovi;
	}

	public String[] getAktivnosti() {
		try {
			if(this.aktivnosti[0] == "" || aktivnosti[0] == " ")
				return new String[0];
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		
		return aktivnosti;
	}

	public void setAktivnosti(String[] aktivnosti) {

		try {
			if(aktivnosti[0] == "" || aktivnosti[0] == " ") {
				ArrayList<String> ds = new ArrayList<String>();
				for (String string : aktivnosti) {
					if(string != "")
						ds.add(string);
				}
				
				String[] odg = new String[ds.size()];
				
				for (int i = 0 ; i < ds.size() ; ++i) {
					odg[i] = ds.get(i);
				}
				
				this.aktivnosti = odg;
			}
			else
				this.aktivnosti = aktivnosti;
		}catch (Exception e) {
			this.aktivnosti = new String[0];
		}
		
		//this.aktivnosti = aktivnosti;
	}

}
