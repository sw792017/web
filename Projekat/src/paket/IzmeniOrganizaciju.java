package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class IzmeniOrganizaciju
 */
@WebServlet("/IzmeniOrganizaciju")
public class IzmeniOrganizaciju extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IzmeniOrganizaciju() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			boolean ponadjen = false;
			HttpSession session = request.getSession();
			Korisnik korisnik = (Korisnik)session.getAttribute("ulogovan");
			Organizacija kk = new Organizacija();
			
			String kogastr = (String)request.getSession().getAttribute("koga"); 
			for(Organizacija kkkk : Organizacija.ucitaj()) {
			 	if(kkkk.getIme().equals(kogastr)){
					kk = kkkk;
					ponadjen = true;
				    break;
			 	}
			}

			ArrayList<Organizacija> lista = Organizacija.ucitaj();
			lista.remove(kk);
			
			boolean postoji = false;
			for(Organizacija kkkk : Organizacija.ucitaj()) {
				if (kkkk.getIme().equals(request.getParameter("ime")))
					postoji = true;
			}

			
			if(!request.getParameter("ime").equals("") && !request.getParameter("ime").equals(kk.getIme()) && korisnik.getUloga().equals("sa") && !postoji) {
				ArrayList<Korisnik> korisnici = Korisnik.ucitajKorisnike();
				for(Korisnik k : korisnici)
					if(k.getOrganizacija().equals(kk.getIme()))
						k.setOrganizacija(request.getParameter("ime"));
				Korisnik.upisiKorisnika(korisnici);
				
				ArrayList<VirtualnaMasina> VMs = VirtualnaMasina.ucitaj();
				for(VirtualnaMasina v : VMs)
					if(v.getOrganizacija().equals(kk.getIme()))
						v.setOrganizacija(request.getParameter("ime"));
				VirtualnaMasina.upisi(VMs);
				
				ArrayList<Disk> diskovi = Disk.ucitaj();
				for(Disk d : diskovi)
					if(d.getOrg().equals(kk.getIme()))
						d.setOrg(request.getParameter("ime"));
				Disk.upisi(diskovi);
				
				kk.setIme(request.getParameter("ime"));
			}
			System.out.print(request.getParameter("logo"));
			
			kk.setOpis(request.getParameter("opis"));//moze da bude prazan
			System.out.print(request.getParameter("logo"));
			
			if(request.getParameter("logo") != "unknown" && request.getParameter("logo") != "" && request.getParameter("logo") != null)
				kk.setLogo("slike/" + request.getParameter("logo"));
			
			lista.add(kk);
			
			if(ponadjen) {
				if(korisnik.getUloga().equals("sa") || (korisnik.getUloga().equals("a") && kk.getIme().equals(korisnik.getOrganizacija()))) {
					Organizacija.upisi(lista);
					
					RequestDispatcher dd=request.getRequestDispatcher("pocetna.jsp");
					dd.forward(request, response);
					return;
				}
				else {
					response.sendError(403);
					return;
				}
			}
			else {
				response.sendError(400);
				return;
			}

		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}

}
