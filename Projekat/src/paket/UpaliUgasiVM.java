package paket;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class UpaliUgasiVM
 */
@WebServlet("/UpaliUgasiVM")
public class UpaliUgasiVM extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpaliUgasiVM() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			boolean ponadjen = false;
			HttpSession session = request.getSession();
			Korisnik korisnik = (Korisnik)session.getAttribute("ulogovan");
			Kategorija kat = new Kategorija("Greska", 0, 0.0);
			VirtualnaMasina kk = new VirtualnaMasina("","", kat);
			
			String kogastr = (String)request.getSession().getAttribute("koga"); 
			for(VirtualnaMasina kkkk : VirtualnaMasina.ucitaj()) {
			 	if(kkkk.getIme().equals(kogastr)){
					kk = kkkk;
					ponadjen = true;
				    break;
			 	}
			}

			ArrayList<VirtualnaMasina> lista = VirtualnaMasina.ucitaj();
			lista.remove(kk);
			int sdrrt = 0;
			for(String s : kk.getDiskovi())
				if(s.length() > 0)
					++sdrrt;
			if(sdrrt != 0)
				kk.addAktivnost(LocalDateTime.now());
			else {
				request.setAttribute("porukaa", "Ne mozete da upalite masinu koja nema ni 1 hard disk");
				RequestDispatcher dd=request.getRequestDispatcher("pogledajVM.jsp");
				dd.forward(request, response);
			}
			lista.add(kk);
			
			if(ponadjen) {
				if(korisnik.getUloga().equals("sa") || (korisnik.getUloga().equals("a") && kk.getIme().equals(korisnik.getOrganizacija()))) {
					VirtualnaMasina.upisi(lista);
					
					/*RequestDispatcher dd=request.getRequestDispatcher("pocetna.jsp");
					dd.forward(request, response);
					return;*/
					request.getSession().setAttribute("koga", kk.getIme());
					response.sendRedirect("pogledajVM.jsp");
					return;
				}
				else {
					response.sendError(403);
					return;
				}
			}
			else {
				response.sendError(400);
				return;
			}

		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}

}
