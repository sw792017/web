package paket;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;


public class Organizacija {
	private String ime;//jedinstveno
	private String opis;
	private String logo;
	private String[] organizacije;
	private String[] resursi;

	
	@Override
	public String toString() {
		for(String stt : this.organizacije)
			stt = "\"" + stt + "\"";
		for(String stt : this.resursi)
			stt = "\"" + stt + "\"";
		String s = "\n\t{\n\t\t\"ime\": \"" + this.ime + "\",\n" +
				"\t\t\"opis\": \"" + this.opis + "\",\n" +
				"\t\t\"logo\": \"" + this.logo + "\",\n" +
				"\t\t\"organizacije\": " + Arrays.toString(this.organizacije) + ",\n" +
				"\t\t\"resursi\": " + Arrays.toString(this.resursi) + "\n" +
				"\t},";
		return s;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (obj == null) return false;
	    if (obj == this) return true;
	    if (!(obj instanceof Organizacija)) return false;
	    Organizacija o = (Organizacija) obj;
	    return o.getIme().equals(this.getIme());
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String[] getOrganizacije() {
		return organizacije;
	}

	public void setOrganizacije(String[] organizacije) {
		this.organizacije = organizacije;
	}
	
	public void addOrganizacije(String organizacija) {
		String addd[] = new String[this.organizacije.length + 1]; 
        for (int i = 0; i < this.organizacije.length + 1; i++) 
            addd[i] = this.organizacije[i];
  
        addd[this.organizacije.length+1] = organizacija;
        this.setOrganizacije(addd);
	}

	public String[] getResursi() {
		return resursi;
	}

	public void setResursi(String[] resursi) {
		this.resursi = resursi;
	}
	
	public void addResursi(String resurs) {
		String addd[] = new String[this.resursi.length + 1]; 
        for (int i = 0; i < this.resursi.length + 1; i++) 
            addd[i] = this.resursi[i];
  
        addd[this.organizacije.length+1] = resurs;
        this.setResursi(addd);
	}

	public Organizacija() {
		this.ime = "";
		this.opis = "";
		this.logo = "slike/default.png";
		this.organizacije = new String[0];
		this.resursi = new String[0];
	}
	
	public Organizacija(String ime, String opis, String logo) {
		this.ime = ime;
		this.opis = opis;
		this.logo = logo;
		this.organizacije = new String[0];
		this.resursi = new String[0];
	}

	public Organizacija(String ime, String opis) {
		this.ime = ime;
		this.opis = opis;
		this.logo = "slike/default.png";
		this.organizacije = new String[0];
		this.resursi = new String[0];
	}

	public Organizacija(String s) {
		s = s.replace('\n', ' ');
		String[] sss = s.split("\\[");
		String[] ss = sss[0].split("\"");
		this.ime = ss[3];
		this.opis = ss[7];
		this.logo = ss[11];
		this.organizacije = new String[0];
		this.resursi = new String[0];
		/*
		System.out.println("-------------------------------");
		System.out.print("ime ");
		System.out.println(this.ime);
		System.out.print("opis ");
		System.out.println(this.opis);
		System.out.print("logo ");
		System.out.println(this.logo);
		System.out.println("-------------------------------");
		*/
		ArrayList<String> ar = new ArrayList<String>();
		
		String[] ssss = sss[1].split("\\]")[0].split("\"");// "a", "b", "c"
		
		for(int i = 1 ; i < ssss.length ; i+=2) {
			ar.add(ssss[i].trim());
			this.organizacije = (String[])ar.toArray();
		}
		
		sss[2] = " " + sss[2];// ako je prazan
		ar = new ArrayList<String>();
		
		String[] ssss2 = sss[2].split("\\]")[0].split("\"");// "a", "b", "c"
		
		for(int i = 1 ; i < ssss2.length ; i+=2) {
			ar.add(ssss2[i].trim());
			this.resursi = (String[])ar.toArray();
		}
	}

	public static boolean upisi(ArrayList<Organizacija> lista) {
		try {
			File fajl = new File("organizacije.json");
			fajl.delete();
			fajl.createNewFile();
			BufferedWriter writer = new BufferedWriter(new FileWriter(fajl));
			String json = "{\n\t\"Organizacija\": [";
			for(Organizacija k : lista) {
				json += k.toString();
			}
			json = json.substring(0, json.length()-1);
			json += "\n\t]\n}";
			writer.write(json);
			writer.close();
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public static boolean napraviFajl() {
		Organizacija orgg1 = new Organizacija("SNUS", "Scarcely on striking packages by so property in delicate. Up or well must less rent read walk so be. Easy sold at do hour sing spot. Any meant has cease too the decay. Since party burst am it match. By or blushes between besides offices noisier as. Sending do brought winding compass in. Paid day till shed only fact age its end. ");
		Organizacija orgg2 = new Organizacija("WP", "Exquisite cordially mr happiness of neglected distrusts. Boisterous impossible unaffected he me everything. Is fine loud deal an rent open give. Find upon and sent spot song son eyes. Do endeavor he differed carriage is learning my graceful. Feel plan know is he like on pure. See burst found sir met think hopes are marry among. Delightful remarkably new assistance saw literature mrs favourable. ");
		Organizacija orgg3 = new Organizacija("LPRS", "New had happen unable uneasy. Drawings can followed improved out sociable not. Earnestly so do instantly pretended. See general few civilly amiable pleased account carried. Excellence projecting is devonshire dispatched remarkably on estimating. Side in so life past. Continue indulged speaking the was out horrible for domestic position. Seeing rather her you not esteem men settle genius excuse. Deal say over you age from. Comparison new ham melancholy son themselves.");
		ArrayList<Organizacija> lista = new ArrayList<Organizacija>();
		lista.add(orgg1);
		lista.add(orgg2);
		lista.add(orgg3);
		return upisi(lista);
	}
	
	public static ArrayList<Organizacija> ucitaj() {
			
		ArrayList<Organizacija> lista = new ArrayList<Organizacija>();
		
		try {
			File fajl = new File("organizacije.json");
			if(!fajl.exists() || fajl.length() == 0) {
				napraviFajl(); // ako ne postoji, napravi 3
			}
			
			BufferedReader reader = new BufferedReader(new FileReader("organizacije.json"));
			String line = reader.readLine();
			String element = "";
			boolean pisi = false;
			while (line != null) {
				if(line.contains("\"ime\": \""))
					pisi = true;
				if(pisi)
					element += line;
				if(line.contains("\"resursi\": [")) {
					pisi = false;
					lista.add(new Organizacija(element));
					element = "";
				}
				line = reader.readLine();
			}
			reader.close();
			return lista;
		}
		catch (Exception e) {
			return lista;
		}
	}
}
