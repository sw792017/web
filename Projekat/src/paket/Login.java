package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String page = "login.jsp";
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		boolean pogresno = true;
		
		ArrayList<Korisnik> lista = Korisnik.ucitajKorisnike();
		
		for(Korisnik k : lista) {
			//System.out.println(k.getEmail() + " - " + email + "\n" + k.getPassword() + " - " + password + "\n" + k.getEmail().equals(email) + k.getPassword().equals(password));
			if(k.getEmail().equals(email) && k.getPassword().equals(password)) {
				page = "pocetna.jsp";
				request.setAttribute("poruka", "");
				HttpSession session = request.getSession();
				session.setAttribute("ulogovan", k);
				session.setAttribute("jeUlogovan", true);
				pogresno = false;
			}
		}
		
		if(pogresno)
			request.setAttribute("poruka", "Pogresno korisnicko ime ili lozinka");
		RequestDispatcher dd=request.getRequestDispatcher(page);
		dd.forward(request, response);
	}

}
