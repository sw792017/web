package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DodajKorisnika
 */
@WebServlet("/DodajKorisnika")
public class DodajKorisnika extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DodajKorisnika() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Korisnik korisnik = new Korisnik();
			HttpSession session = request.getSession();
			Korisnik kk = (Korisnik)session.getAttribute("ulogovan");
			String page = "pocetna.jsp";
			boolean pogresno = false;
			
			korisnik.setEmail(request.getParameter("email"));
			korisnik.setPassword(request.getParameter("password"));
			korisnik.setIme(request.getParameter("ime"));
			korisnik.setPrezime(request.getParameter("prezime"));
			String ss;
			
			if(kk.getUloga().equals("sa"))// ako nije super admin, polje ne postoji
				ss = request.getParameter("organizacija");
			else
				ss = kk.getOrganizacija();
			korisnik.setOrganizacija(ss);
			korisnik.setUloga(request.getParameter("uloga"));
			
			ArrayList<Korisnik> lista = Korisnik.ucitajKorisnike();
			
			for(Korisnik k : lista) {
				if(k.getEmail().equals(korisnik.getEmail())) {
					page = "dodajKorisnika.jsp";
					pogresno = true;
				}
			}
			
			if(pogresno) {
				request.setAttribute("poruka", "Email je vez zauzet");
				//response.sendError(400);//da li je ovo bolje od vracanja na situ stranu sa porukom?
				//return;
				RequestDispatcher dd=request.getRequestDispatcher(page);
				dd.forward(request, response);
				return;
			}
			else
				request.setAttribute("poruka", "");
	
			if(kk.getUloga().equals("sa") || kk.getUloga().equals("a") && (!(korisnik.getEmail().equals("") || korisnik.getIme().equals("") || korisnik.getOrganizacija().equals("") || korisnik.getPassword().equals("") || korisnik.getPrezime().equals("") || korisnik.getUloga().equals("")))) {
				ArrayList<Korisnik> kor = new ArrayList<Korisnik>();
				kor = Korisnik.ucitajKorisnike();
				kor.add(korisnik);
				Korisnik.upisiKorisnika(kor);
				
				RequestDispatcher dd=request.getRequestDispatcher(page);
				dd.forward(request, response);
				return;
			}
			else {// do ovog dela se moze doci samo ako se admin izloguje sa drugog taba dok je ova stranica ucitana
				response.sendError(403);
				return;
			}
		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}
}
