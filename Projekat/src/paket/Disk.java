package paket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class Disk {
	private String ime;
	private String org;
	private String tip;// (SSD ili HDD)
	private double kapacitet;
	private String vm;
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
	    if (obj == this) return true;
	    if (!(obj instanceof Disk)) return false;
	    Disk o = (Disk) obj;
	    return o.getIme().equals(this.getIme());
	}
	@Override
	public String toString() {
		String s = "\n\t{\n\t\t\"ime\": \"" + this.ime + "\",\n" +
				"\t\t\"org\": \"" + this.org + "\",\n" +
				"\t\t\"tip\": \"" + this.tip + "\",\n" +
				"\t\t\"kapacitet\": \"" + this.kapacitet + "\",\n" +
				"\t\t\"vm\": \"" + this.vm + "\",\n" +
				"\t},";
		return s;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getOrg() {
		return org;
	}
	public void setOrg(String org) {
		this.org = org;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = "HDD";
		if (tip.toUpperCase().equals("SSD") || tip.toUpperCase().equals("HDD"))
			this.tip = tip.toUpperCase();
	}
	public double getKapacitet() {
		return kapacitet;
	}
	public void setKapacitet(double kapacitet) {
		this.kapacitet = kapacitet;
	}
	public String getVm() {
		return vm;
	}
	public void setVm(String vm) {
		this.vm = vm;
	}
	public Disk(String ime, String org, String tip, double kapacitet, String vm) {
		this.ime = ime;
		this.org = org;
		this.tip = tip.toUpperCase();
		if (!(tip.toUpperCase().equals("SSD") || tip.toUpperCase().equals("HDD")))
			this.tip = "HDD";
		this.kapacitet = kapacitet;
		this.vm = vm;
	}
	
	public Disk(String ime, String org, String tip, double kapacitet) {
		this.ime = ime;
		this.org = org;
		this.tip = tip.toUpperCase();
		if (!(tip.toUpperCase().equals("SSD") || tip.toUpperCase().equals("HDD")))
			this.tip = "HDD";
		this.kapacitet = kapacitet;
		this.vm = "";
	}
	
	public Disk(String ime, String org, String vm) {
		this.ime = ime;
		this.org = org;
		this.tip = "HDD";
		this.kapacitet = 0;
		this.vm = vm;
	}
	
	public Disk(String s) {
		s = s.replace('\n', ' ');
		String[] ss = s.split("\"");
		this.ime = ss[3];
		String org = "";
		//////////////////////////////////
		for(Organizacija org1 : Organizacija.ucitaj())
			if(org1.getIme().equals(ss[7]))
				org = org1.getIme();
		this.org = org;
		//////////////////////////////////
		this.tip = ss[11];
		this.kapacitet = Double.valueOf(ss[15]);
		////////////////////////

		String vm = "";
		try {
			for(VirtualnaMasina vm1 : VirtualnaMasina.ucitaj())
				if(vm1.getIme().equals(ss[19]))
					vm = vm1.getIme();
			this.vm = vm;
		}catch (Exception e) {this.vm = "";}

	}
	
	//////////////////////////////////////////////////////////////////////////////////////////
	
	public static boolean upisi(ArrayList<Disk> lista) {
		try {
			File fajl = new File("diskovi.json");
			fajl.delete();
			fajl.createNewFile();
			BufferedWriter writer = new BufferedWriter(new FileWriter(fajl));
			String json = "{\n\t\"Disk\": [";
			for(Disk k : lista) {
				json += k.toString();
			}
			json = json.substring(0, json.length()-1);
			json += "\n\t]\n}";
			writer.write(json);
			writer.close();
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public static boolean napraviFajl() {
		ArrayList<Disk> lista = new ArrayList<Disk>();
		return upisi(lista);
	}
	
	public static ArrayList<Disk> ucitaj() {
		
		ArrayList<Disk> lista = new ArrayList<Disk>();
		
		try {
			File fajl = new File("diskovi.json");
			if(!fajl.exists() || fajl.length() == 0) {
				napraviFajl(); // ako ne postoji
			}
			
			BufferedReader reader = new BufferedReader(new FileReader("diskovi.json"));
			String line = reader.readLine();
			String element = "";
			boolean pisi = false;
			while (line != null) {
				if(line.contains("\"ime\": \""))
					pisi = true;
				if(pisi)
					element += line;
				if(line.contains("\"vm\": \"")) {
					pisi = false;
					lista.add(new Disk(element));
					element = "";
				}
				//System.out.println(line);
				line = reader.readLine();
			}
			reader.close();
			return lista;
		}
		catch (Exception e) {
			return lista;
		}
	}
}
