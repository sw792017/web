package paket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DodajVM
 */
@WebServlet("/DodajVM")
public class DodajVM extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DodajVM() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Korisnik kk = (Korisnik)session.getAttribute("ulogovan");
			String page = "pocetna.jsp";
			boolean pogresno = false;
			
			String ime = request.getParameter("ime");
			
			String org = request.getParameter("organizacija");
			Organizacija organizacija = new Organizacija();
			for(Organizacija o : Organizacija.ucitaj())
				if(o.getIme().equals(org))
					organizacija = o;
			
			String kat = request.getParameter("kategorija");
			Kategorija kategorija = new Kategorija("", 0, 0);
			for(Kategorija k : Kategorija.ucitaj())
				if(k.getIme().equals(kat))
					kategorija = k;
			
			
			if(ime=="" || organizacija.getIme() == "" || kategorija.getIme() == "") {
				RequestDispatcher ddr=request.getRequestDispatcher(page);
				ddr.forward(request, response);
				return;
			}
			VirtualnaMasina dd = new VirtualnaMasina(ime, org, kategorija);	
			ArrayList<VirtualnaMasina> lista = VirtualnaMasina.ucitaj();
			
			for(VirtualnaMasina d : lista) {
				if(d.getIme().equals(dd.getIme())) {
					page = "dodajVM.jsp";
					pogresno = true;
				}
			}
			
			if(pogresno) {
				request.setAttribute("poruka", "Ime je vez zauzeto");
				//response.sendError(400);//da li je ovo bolje od vracanja na situ stranu sa porukom?
				//return;
				RequestDispatcher ddr=request.getRequestDispatcher(page);
				ddr.forward(request, response);
				return;
			}
			else
				request.setAttribute("poruka", "");

			//Selektovani diskovi
			for(Disk kkk : Disk.ucitaj()) {
				if (request.getParameter(kkk.getIme()) != null){
					dd.addDisk(kkk.getIme());
				 }
			}
			/*
			ArrayList<Disk> diskovi = Disk.ucitaj();
			
			for(Disk d : diskovi) {
				if(d.getOrg().equals(dd.getOrganizacija())) {
					List<String> list = Arrays.asList(dd.getDiskovi());
					if(list.contains(dd.getIme()))
						d.setVm(dd.getIme());
				}
			}
			*/
			
			ArrayList<Disk> diskovi = Disk.ucitaj();

			
			for(Disk d : diskovi) {
				if(d.getOrg().equals(dd.getOrganizacija())) {
					for(String sssss : dd.getDiskovi())
						if(sssss.equals(d.getIme()))
							d.setVm(dd.getIme());
				}
			}
			Disk.upisi(diskovi);
			
			if(kk.getUloga().equals("sa") || kk.getUloga().equals("a") && (!(dd.getIme().equals("")))) {
				ArrayList<VirtualnaMasina> ko = new ArrayList<VirtualnaMasina>();
				ko = VirtualnaMasina.ucitaj();
				ko.add(dd);
				VirtualnaMasina.upisi(ko);
				
				RequestDispatcher ddr=request.getRequestDispatcher(page);
				ddr.forward(request, response);
				return;
			}
			else {
				response.sendError(403);
				return;
			}
		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}

}
