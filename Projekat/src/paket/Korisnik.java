package paket;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;


public class Korisnik {
	private String email;
	private String password;
	private String ime;
	private String prezime;
	private String organizacija;
	private String uloga;
	
	@Override
	public String toString() {
		String s = "\n\t{\n\t\t\"email\": \"" + this.email + "\",\n" +
				"\t\t\"password\": \"" + this.password + "\",\n" +
				"\t\t\"ime\": \"" + this.ime + "\",\n" +
				"\t\t\"prezime\": \"" + this.prezime + "\",\n" +
				"\t\t\"organizacija\": \"" + this.organizacija + "\",\n" +
				"\t\t\"uloga\": \"" + this.uloga + "\"\n" +
				"\t},";
		return s;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (obj == null) return false;
	    if (obj == this) return true;
	    if (!(obj instanceof Korisnik)) return false;
	    Korisnik o = (Korisnik) obj;
	    return o.getEmail().equals(this.getEmail());
	}

	public Korisnik() {
		this.email = "";
		this.password = "";
		this.ime = "";
		this.prezime = "";
		this.organizacija = "";
		this.uloga = "";
	}

	public Korisnik(String email, String password, String ime, String prezime, String organizacija, String uloga) {
		this.email = email;
		this.password = password;
		this.ime = ime;
		this.prezime = prezime;
		this.organizacija = organizacija;
		this.uloga = uloga;
	}
	
	public Korisnik(String s) {
		s = s.replace('\n', ' ');
		String[] ss = s.split("\"");
		this.email = ss[3];
		this.password = ss[7];
		this.ime = ss[11];
		this.prezime = ss[15];
		this.organizacija = ss[19];
		this.uloga = ss[23];
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getOrganizacija() {
		return organizacija;
	}

	public void setOrganizacija(String organizacija) {
		this.organizacija = organizacija;
	}

	public String getUloga() {
		return uloga;
	}

	public void setUloga(String uloga) {
		this.uloga = uloga;
	}
	
	public static boolean upisiKorisnika(ArrayList<Korisnik> lista) {
		try {
			File fajl = new File("korisnici.json");
			fajl.delete();
			fajl.createNewFile();
			BufferedWriter writer = new BufferedWriter(new FileWriter(fajl));
			String json = "{\n\t\"Korisnik\": [";
			for(Korisnik k : lista) {
				json += k.toString();
			}
			json = json.substring(0, json.length()-1);
			json += "\n\t]\n}";
			writer.write(json);
			writer.close();
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public static boolean napraviFajl() {
		Korisnik superAdmin = new Korisnik("superadmin@email.com", "password", "Admin", "Admin", "", "sa");
		ArrayList<Korisnik> lista = new ArrayList<Korisnik>();
		lista.add(superAdmin);
		return upisiKorisnika(lista);
	}
	
	public static ArrayList<Korisnik> ucitajKorisnike() {
		
		ArrayList<Korisnik> lista = new ArrayList<Korisnik>();
		
		try {
			File fajl = new File("korisnici.json");
			if(!fajl.exists() || fajl.length() == 0) {
				napraviFajl(); // ako ne postoji
			}
			
			BufferedReader reader = new BufferedReader(new FileReader("korisnici.json"));
			String line = reader.readLine();
			String element = "";
			boolean pisi = false;
			while (line != null) {
				if(line.contains("\"email\": \""))
					pisi = true;
				if(pisi)
					element += line;
				if(line.contains("\"uloga\": \"")) {
					pisi = false;
					lista.add(new Korisnik(element));
					element = "";
				}
				
				line = reader.readLine();
			}
			reader.close();
			
			/*JSONParser parser=new JSONParser();
			JSONArray a = (JSONArray)parser.parse(new FileReader("korisnici.json"));
	
			for (Object o : a){
				JSONObject person = (JSONObject) o;
		
				String email = (String) person.get("email");
				System.out.println(email);
		
				String password = (String) person.get("password");
				System.out.println(password);
		
				String ime = (String) person.get("ime");
				System.out.println(ime);
				
				String prezime = (String) person.get("prezime");
				System.out.println(prezime);
		
				String organizacija = (String) person.get("organizacija");
				System.out.println(organizacija);
		
				String uloga = (String) person.get("uloga");
				System.out.println(uloga);
			}*/
			return lista;
		}
		catch (Exception e) {
			return lista;
		}
	}
}
