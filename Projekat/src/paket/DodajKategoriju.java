package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DodajKategoriju
 */
@WebServlet("/DodajKategoriju")
public class DodajKategoriju extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DodajKategoriju() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Korisnik kk = (Korisnik)session.getAttribute("ulogovan");
			String page = "pocetna.jsp";
			boolean pogresno = false;
			
			String ime = request.getParameter("ime");
			int brj = (int) Math.floor(Double.parseDouble(request.getParameter("brj")));
			int brgj = (int) Math.floor(Double.parseDouble(request.getParameter("brgj")));
			double ram = Double.parseDouble(request.getParameter("ram"));
			
			Kategorija kat = new Kategorija(ime, brj, ram, brgj);			
			ArrayList<Kategorija> lista = Kategorija.ucitaj();
			
			for(Kategorija k : lista) {
				if(k.getIme().equals(kat.getIme())) {
					page = "dodajKategoriju.jsp";
					pogresno = true;
				}
			}
			
			if(pogresno) {
				request.setAttribute("poruka", "Ime je vez zauzeto");
				//response.sendError(400);//da li je ovo bolje od vracanja na situ stranu sa porukom?
				//return;
				RequestDispatcher dd=request.getRequestDispatcher(page);
				dd.forward(request, response);
				return;
			}
			else
				request.setAttribute("poruka", "");
	
			if(kk.getUloga().equals("sa") || kk.getUloga().equals("a") && (!(kat.getIme().equals("") || kat.getBrJezgra()<1 || kat.getBrGPUJezgra()<0 || kat.getRam()<=0 ))) {
				ArrayList<Kategorija> ko = new ArrayList<Kategorija>();
				ko = Kategorija.ucitaj();
				ko.add(kat);
				Kategorija.upisi(ko);
				
				RequestDispatcher dd=request.getRequestDispatcher(page);
				dd.forward(request, response);
				return;
			}
			else {
				response.sendError(403);
				return;
			}
		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}
}
