package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ObrisiDisk
 */
@WebServlet("/ObrisiDisk")
public class ObrisiDisk extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ObrisiDisk() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			boolean ponadjen = false;
			HttpSession session = request.getSession();
			Korisnik korisnik = (Korisnik)session.getAttribute("ulogovan");
			
			Disk kk = new Disk("", "", "");
			
			String kogastr = (String)request.getSession().getAttribute("koga"); 
			for(Disk kkkk : Disk.ucitaj()) {
			 	System.out.println(kkkk.getIme()); System.out.println(" == "); System.out.println(kogastr);
			 	if(kkkk.getIme().equals(kogastr)){
					kk = kkkk;
					ponadjen = true;
				    break;
			 	}
			}
			
			ArrayList<Disk> lista = Disk.ucitaj();
			lista.remove(kk);
			
			if(ponadjen) {
				if(korisnik.getUloga().equals("sa")) {
					Disk.upisi(lista);
					
					RequestDispatcher dd=request.getRequestDispatcher("pocetna.jsp");
					dd.forward(request, response);
					return;
				}
				else {
					response.sendError(403);
					return;
				}
			}
			else {
				response.sendError(400);
				return;
			}

		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}

}
