package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DodajDisk
 */
@WebServlet("/DodajDisk")
public class DodajDisk extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DodajDisk() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Korisnik kk = (Korisnik)session.getAttribute("ulogovan");
			String page = "pocetna.jsp";
			boolean pogresno = false;
			
			String ime = request.getParameter("ime");
			double kapacitet = Double.parseDouble(request.getParameter("kapacitet"));
			String org = request.getParameter("organizacija");
			String tip = request.getParameter("tip");
			
			Disk dd = new Disk(ime, org, tip, kapacitet);			
			ArrayList<Disk> lista = Disk.ucitaj();
			
			for(Disk d : lista) {
				if(d.getIme().equals(dd.getIme())) {
					page = "dodajDisk.jsp";
					pogresno = true;
				}
			}
			
			if(pogresno) {
				request.setAttribute("poruka", "Ime je vez zauzeto");
				//response.sendError(400);//da li je ovo bolje od vracanja na situ stranu sa porukom?
				//return;
				RequestDispatcher ddr=request.getRequestDispatcher(page);
				ddr.forward(request, response);
				return;
			}
			else
				request.setAttribute("poruka", "");
			
			if(kk.getUloga().equals("a")) {
				dd.setOrg(kk.getOrganizacija());
			}
			else {
				dd.setOrg(org);
			}
	
			if(kk.getUloga().equals("sa") || kk.getUloga().equals("a") && (!(dd.getIme().equals("") || dd.getKapacitet()<0.1 ))) {
				ArrayList<Disk> ko = new ArrayList<Disk>();
				ko = Disk.ucitaj();
				ko.add(dd);
				Disk.upisi(ko);
				
				RequestDispatcher ddr=request.getRequestDispatcher(page);
				ddr.forward(request, response);
				return;
			}
			else {
				response.sendError(403);
				return;
			}
		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}

}
