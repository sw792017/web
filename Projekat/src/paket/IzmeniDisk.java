package paket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class IzmeniDisk
 */
@WebServlet("/IzmeniDisk")
public class IzmeniDisk extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IzmeniDisk() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// za svaki VM .replace |ime| sa |novo ime|
		try {
			boolean ponadjen = false;
			HttpSession session = request.getSession();
			Korisnik korisnik = (Korisnik)session.getAttribute("ulogovan");
			Disk dd = new Disk("", "", "");
			
			String kogastr = (String)request.getSession().getAttribute("koga"); 
			for(Disk ddd : Disk.ucitaj()) {
			 	if(ddd.getIme().equals(kogastr)){
					dd = ddd;
					ponadjen = true;
				    break;
			 	}
			}
			
			double kap = Double.parseDouble(request.getParameter("kapacitet"));
			
			if((double)kap > 0)
				dd.setKapacitet(kap);

			ArrayList<Disk> lista = Disk.ucitaj();
			lista.remove(dd);
			boolean postoji = false;
			for(Disk ddd : Disk.ucitaj()) {
				if (ddd.getIme().equals(request.getParameter("ime")))
					postoji = true;
			}
			if(!(postoji || dd.getIme().equals(""))) {
				ArrayList<VirtualnaMasina> v = VirtualnaMasina.ucitaj();
				for (VirtualnaMasina vv : v) {
					List<String> str = Arrays.asList(vv.getDiskovi());
					if(str.contains(dd.getIme())) {
						str.remove(dd.getIme());
						str.add(request.getParameter("ime"));
						vv.setDiskovi((String[])str.toArray());
					}
				}
				VirtualnaMasina.upisi(v);
				dd.setIme(request.getParameter("ime"));
			}
			
			double kapacitet = Double.parseDouble(request.getParameter("kapacitet"));

			String tip = request.getParameter("tip");
			
			if(kapacitet >= 0.1)
				dd.setKapacitet(kapacitet);
			if(tip != "") {
				dd.setTip(tip.toUpperCase());
				/*if(tip.toUpperCase().equals("SSD") || tip.toUpperCase().equals("HDD"))
					dd.setTip(tip.toUpperCase());*/
			}
			System.out.println(tip.toUpperCase() + " == " + (tip.toUpperCase().equals("SSD") || tip.toUpperCase().equals("HDD")));
			
			
				dd.setVm(request.getParameter("VMasina"));
				ArrayList<VirtualnaMasina> VMs = VirtualnaMasina.ucitaj();
				for(VirtualnaMasina vm : VMs) {
					vm.setDiskovi(new String[0]);
					
					if(dd.getVm().equals(vm.getIme()))
						vm.addDisk(dd.getIme());
					
					for(Disk d : Disk.ucitaj()) {
						if(d.getVm().equals(vm.getIme()) && !d.getIme().equals(dd.getIme()))
							vm.addDisk(d.getIme());
						
					}
				}
				
				VirtualnaMasina.upisi(VMs);
			
			lista.add(dd);
			
			if(ponadjen) {
				if(korisnik.getUloga().equals("sa") || (korisnik.getUloga().equals("a"))) {
					Disk.upisi(lista);
					
					RequestDispatcher ddr=request.getRequestDispatcher("pocetna.jsp");
					ddr.forward(request, response);
					return;
				}
				else {
					response.sendError(403);
					return;
				}
			}
			else {
				response.sendError(400);
				return;
			}

		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}

}
