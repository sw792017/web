	package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Pogledaj
 */
@WebServlet("/Pogledaj")
public class Pogledaj extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Pogledaj() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Korisnik k = (Korisnik)session.getAttribute("ulogovan");
			ArrayList<Korisnik> kor = Korisnik.ucitajKorisnike();
			System.out.println(k.toString());
			Korisnik kk = new Korisnik();
			for(Korisnik kkk : kor) {
				if (request.getParameter(kkk.getEmail()) != null){
				       kk = kkk;
				       break;
				 }
			}
			System.out.println(kk.toString());
			if((k.getEmail() != kk.getEmail())) {
				if( ( k.getUloga().equals("a") && kk.getUloga().equals("k") && k.getOrganizacija().equals(kk.getOrganizacija()) ) || (k.getUloga().equals("sa") && (!kk.getUloga().equals("sa"))) ) {
					request.getSession().setAttribute("koga", kk.getEmail());
					response.sendRedirect("pogledaj.jsp");
					return;
				}
				else {
					response.sendError(403);
					return;
				}
			}
			else {
				response.sendError(400);
				return;
			}
				
		}catch (Exception e) {
			response.sendError(403);
			return;
		}
	}
}
