package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class IzmeniKategoriju
 */
@WebServlet("/IzmeniKategoriju")
public class IzmeniKategoriju extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IzmeniKategoriju() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			boolean ponadjen = false;
			HttpSession session = request.getSession();
			Korisnik korisnik = (Korisnik)session.getAttribute("ulogovan");
			Kategorija kk = new Kategorija("", 0, 0);
			
			String kogastr = (String)request.getSession().getAttribute("koga"); 
			for(Kategorija kkkk : Kategorija.ucitaj()) {
			 	if(kkkk.getIme().equals(kogastr)){
					kk = kkkk;
					ponadjen = true;
				    break;
			 	}
			}
			
			int brj = (int) Math.floor(Double.parseDouble(request.getParameter("brj")));
			int brgj = (int) Math.floor(Double.parseDouble(request.getParameter("brgj")));
			double ram = Double.parseDouble(request.getParameter("ram"));
			
			if((int)brj >= 1)
				kk.setBrGPUJezgra(brgj);
			if((int)brgj >= 0)
				kk.setBrJezgra(brj);
			if((double)ram > 0)
				kk.setRam(ram);

			ArrayList<Kategorija> lista = Kategorija.ucitaj();
			lista.remove(kk);
			boolean postoji = false;
			for(Kategorija kkkk : Kategorija.ucitaj()) {
				if (kkkk.getIme().equals(request.getParameter("ime")))
					postoji = true;
			}
			if(!(postoji || kk.getIme().equals(""))) {
				ArrayList<VirtualnaMasina> v = VirtualnaMasina.ucitaj();
				for (VirtualnaMasina vv : v) {
					if(vv.getKategorija().getIme().equals(kk.getIme())) {
						vv.setKategorija(kk);
					}
				}
				VirtualnaMasina.upisi(v);
				kk.setIme(request.getParameter("ime"));
			}
			
			
			lista.add(kk);
			
			if(ponadjen) {
				if(korisnik.getUloga().equals("sa") || (korisnik.getUloga().equals("a"))) {
					Kategorija.upisi(lista);
					
					RequestDispatcher dd=request.getRequestDispatcher("pocetna.jsp");
					dd.forward(request, response);
					return;
				}
				else {
					response.sendError(403);
					return;
				}
			}
			else {
				response.sendError(400);
				return;
			}

		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}

}
