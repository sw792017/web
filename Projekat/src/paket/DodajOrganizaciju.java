package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DodajOrganizaciju
 */
@WebServlet("/DodajOrganizaciju")
public class DodajOrganizaciju extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DodajOrganizaciju() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Korisnik korisnik = (Korisnik)session.getAttribute("ulogovan");
			
			Organizacija org = new Organizacija();
			String page = "pocetna.jsp";
			boolean pogresno = false;
			
			org.setIme(request.getParameter("ime"));
			org.setOpis(request.getParameter("opis"));
			if(request.getParameter("logo") != null)
				org.setLogo("slike/" + request.getParameter("logo"));

			ArrayList<Organizacija> lista = Organizacija.ucitaj();
			
			for(Organizacija k : lista) {
				if(k.getIme().equals(org.getIme())) {
					page = "dodajOrganizaciju.jsp";
					pogresno = true;
				}
			}
			
			if(pogresno) {
				request.setAttribute("poruka", "Firam sa ovim imenom vec postoji");
				//response.sendError(400);//da li je ovo bolje od vracanja na situ stranu sa porukom?
				//return;
				RequestDispatcher dd=request.getRequestDispatcher(page);
				dd.forward(request, response);
				return;
			}
			else
				request.setAttribute("poruka", "");
	
			if(korisnik.getUloga().equals("sa")) {
				ArrayList<Organizacija> kor = Organizacija.ucitaj();
				kor.add(org);
				Organizacija.upisi(kor);
				
				RequestDispatcher dd=request.getRequestDispatcher(page);
				dd.forward(request, response);
				return;
			}
			else {// do ovog dela se moze doci samo ako se admin izloguje sa drugog taba dok je ova stranica ucitana
				response.sendError(403);
				return;
			}
		} catch (Exception e) {
			response.sendError(403);//nije ulogovan
			return;
		}
	}

}
