package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class PogledajDisk
 */
@WebServlet("/PogledajDisk")
public class PogledajDisk extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PogledajDisk() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*try {
			HttpSession session = request.getSession();
			Korisnik k = (Korisnik)session.getAttribute("ulogovan");
			Disk kk = new Disk("", "", "");
			for(Disk kkk : Disk.ucitaj()) {
				System.out.println(kkk.getIme() + " iz " + kkk.getOrg() + " postoji? " + request.getParameter(kkk.getIme())==null);
				if (request.getParameter(kkk.getIme()) != null){
				       kk = kkk;
				       break;
				 }
			}
			if(kk.getIme().equals("")) {
				response.sendError(400);
				return;
			}
			System.out.println(kk.toString());
				if( k.getUloga().equals("sa") || (k.getUloga().equals("a"))) {
					request.getSession().setAttribute("koga", kk.getIme());
					response.sendRedirect("pogledajDisk.jsp");
					return;
				}
				else {
					response.sendError(403);
					return;
				}
				
		}catch (Exception e) {
			response.sendError(400);
			return;
		}*/
		try {
			HttpSession session = request.getSession();
			Korisnik k = (Korisnik)session.getAttribute("ulogovan");
			ArrayList<Disk> kor = Disk.ucitaj();

			Disk kk = new Disk("", "", "");
			for(Disk kkk : kor) {
				if (request.getParameter(kkk.getIme()) != null){
				       kk = kkk;
				       break;
				 }
			}

			if(kk.getIme().equals("")) {
				response.sendError(400);
				return;
			}
			if( k.getUloga().equals("sa") || kk.getOrg().equals(k.getOrganizacija())) {
				request.getSession().setAttribute("koga", kk.getIme());
				response.sendRedirect("pogledajDisk.jsp");
				return;
			}
			else {
				response.sendError(403);
				return;
			}
				
		}catch (Exception e) {
			response.sendError(403);
			return;
		}
	}

}
