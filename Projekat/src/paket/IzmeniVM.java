package paket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class IzmeniVM
 */
@WebServlet("/IzmeniVM")
public class IzmeniVM extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IzmeniVM() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			boolean ponadjen = false;
			HttpSession session = request.getSession();
			Korisnik korisnik = (Korisnik)session.getAttribute("ulogovan");
			Kategorija kat = new Kategorija("Greska", 0, 0.0);
			VirtualnaMasina kk = new VirtualnaMasina("","", kat);
			
			String kogastr = (String)request.getSession().getAttribute("koga"); 
			for(VirtualnaMasina kkkk : VirtualnaMasina.ucitaj()) {
			 	if(kkkk.getIme().equals(kogastr)){
					kk = kkkk;
					ponadjen = true;
				    break;
			 	}
			}

			ArrayList<VirtualnaMasina> lista = VirtualnaMasina.ucitaj();
			lista.remove(kk);
			
			boolean postoji = false;
			for(VirtualnaMasina kkkk : VirtualnaMasina.ucitaj()) {
				if (kkkk.getIme().equals(request.getParameter("ime")))
					postoji = true;
			}
			
			if(!request.getParameter("ime").equals("") && !request.getParameter("ime").equals(kk.getIme()) && !korisnik.getUloga().equals("k") && !postoji) {
				kk.setIme(request.getParameter("ime"));
			}
			
			if(!request.getParameter("kat").equals("") && !request.getParameter("kat").equals(kk.getKategorija().getIme()) && !korisnik.getUloga().equals("k")) {
				for(Kategorija k : Kategorija.ucitaj())
					if(k.getIme().equals(request.getParameter("kat")))
						kat = k;
				if(kat.getBrJezgra()!=0)
					kk.setKategorija(kat);
			}
			
			for(int i = 0; i < kk.getDiskovi().length ; ++i) {
				String sl[] = kk.getDiskovi();
				String s = request.getParameter("disk" + (i+1));
				if(s!= null && s!= "") {
					sl[i] = s;
				}
				kk.setDiskovi(sl);
			}
			
			for(int i = 0; i < kk.getAktivnosti().length ; ++i) {
				String sl[] = kk.getAktivnosti();
				String s = request.getParameter("aktivnost" + (i+1));
				if(s!= null && s!= "") {
					sl[i] = s;
				}
				kk.setAktivnosti(sl);
			}
			
			kk.setDiskovi(new String[0]);
			for(Disk kkk : Disk.ucitaj()) {
				if (request.getParameter(kkk.getIme()) != null){
					kk.addDisk(kkk.getIme());
				 }
			}

			ArrayList<Disk> diskovi = Disk.ucitaj();
			
			for(Disk d : diskovi) {
				if(d.getOrg().equals(kk.getOrganizacija())) {
					boolean b = true;
					for(String sssss : kk.getDiskovi())
						if(sssss.equals(d.getIme())) {
							d.setVm(kk.getIme());
							b = false;
						}
					
					if(d.getVm().equals(kk.getIme()) && b)
						d.setVm("");
				}
			}
			
			Disk.upisi(diskovi);
			
			lista.add(kk);
			
			if(ponadjen) {
				if(korisnik.getUloga().equals("sa") || (korisnik.getUloga().equals("a") && kk.getIme().equals(korisnik.getOrganizacija()))) {
					VirtualnaMasina.upisi(lista);
					
					RequestDispatcher dd=request.getRequestDispatcher("pocetna.jsp");
					dd.forward(request, response);
					return;
				}
				else {
					response.sendError(403);
					return;
				}
			}
			else {
				response.sendError(400);
				return;
			}

		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}

}
