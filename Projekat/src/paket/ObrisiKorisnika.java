package paket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ObrisiKorisnika
 */
@WebServlet("/ObrisiKorisnika")
public class ObrisiKorisnika extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ObrisiKorisnika() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			boolean ponadjen = false;
			HttpSession session = request.getSession();
			Korisnik korisnik = (Korisnik)session.getAttribute("ulogovan");
			System.out.println(korisnik.toString());
			Korisnik kk = new Korisnik();
			
			String kogastr = (String)request.getSession().getAttribute("koga"); 
			for(Korisnik kkkk : Korisnik.ucitajKorisnike()) {
			 	System.out.println(kkkk.getEmail()); System.out.println(" == "); System.out.println(kogastr);
			 	if(kkkk.getEmail().equals(kogastr)){
					kk = kkkk;
					ponadjen = true;
				    break;
			 	}
			}
			
			ArrayList<Korisnik> lista = Korisnik.ucitajKorisnike();
			lista.remove(kk);
			
			if(ponadjen) {
				if((korisnik.getUloga().equals("sa") || (korisnik.getUloga().equals("a") && kk.getOrganizacija().equals(korisnik.getOrganizacija()))) && !kk.getEmail().equals(korisnik.getEmail())) {
					Korisnik.upisiKorisnika(lista);
					
					RequestDispatcher dd=request.getRequestDispatcher("pocetna.jsp");
					dd.forward(request, response);
					return;
				}
				else {
					response.sendError(403);
					return;
				}
			}
			else {
				response.sendError(400);
				return;
			}

		} catch (Exception e) {
			response.sendError(403);
			return;
		}
	}

}
